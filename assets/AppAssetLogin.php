<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
// use yii\web\View;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetLogin extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

            "AdminLTE-3.0.0-alpha.2/plugins/font-awesome/css/font-awesome.min.css",
            "AdminLTE-3.0.0-alpha.2/plugins/ionicons/css/ionicons.min.css",
            "AdminLTE-3.0.0-alpha.2/dist/css/adminlte.min.css",
    ];
    public $js = [

            // "AdminLTE-3.0.0-alpha.2/plugins/jquery/jquery.min.js",
            "AdminLTE-3.0.0-alpha.2/plugins/bootstrap/js/bootstrap.bundle.min.js",
    ];
    public $depends = [
//        'yii\web\YiiAsset',

// 'yii\bootstrap\BootstrapAsset',
    ];

    // public function init()
    // {
    //     $this->jsOptions['position'] = View::POS_HEAD;
    //     parent::init();
    // }
}
