<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FrontAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'han/assets/css/bootstrap.min.css',
        'han/assets/revolution/css/settings.css',
        'han/assets/revolution/css/layers.css',
        'han/assets/revolution/css/navigation.css',
        'han/assets/css/magnific-popup.css',
        'han/assets/css/jquery.mmenu.css',
        'han/assets/css/owl.carousel.min.css',
        'han/assets/css/style.css',
        'han/assets/css/style2.css',
        'han/assets/css/responsive.css',
        'han/assets/css/rjcustoms.css',
        'han/assets/css/animate.css',
//        'https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js',
//        'https://oss.maxcdn.com/respond/1.4.2/respond.min.js',
        'han/',
    ];
    public $js = [
        'han/assets/js/jquery-3.2.1.min.js',
        'han/assets/js/bootstrap.min.js',
        'han/assets/js/jquery.ajaxchimp.js',
        'han/assets/js/jquery.magnific-popup.min.js',
        'han/assets/js/jquery.mmenu.js',
        'han/assets/js/jquery.inview.min.js',
        'han/assets/js/wow.min.js',
        'han/assets/js/jquery.countTo.min.js',
        'han/assets/js/jquery.countdown.min.js',
        'han/assets/js/owl.carousel.min.js',
        'han/assets/js/imagesloaded.pkgd.min.js',
        'han/assets/js/isotope.pkgd.min.js',
        'han/assets/js/headroom.js',
        'han/assets/js/custom.js',
        'han/assets/js/video2.js',
        'han/assets/revolution/js/jquery.themepunch.tools.min.js',
        'han/assets/revolution/js/jquery.themepunch.revolution.min.js',
        'han/assets/revolution/js/extensions/revolution.extension.actions.min.js',
        'han/assets/revolution/js/extensions/revolution.extension.carousel.min.js',
        'han/assets/revolution/js/extensions/revolution.extension.kenburn.min.js',
        'han/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js',
        'han/assets/revolution/js/extensions/revolution.extension.migration.min.js',
        'han/assets/revolution/js/extensions/revolution.extension.navigation.min.js',
        'han/assets/revolution/js/extensions/revolution.extension.parallax.min.js',
        'han/assets/revolution/js/extensions/revolution.extension.slideanims.min.js'
    ];
    public $depends = [
//        'yii\web\YiiAsset',
    ];

    public function init()
    {
        $this->jsOptions['position'] = View::POS_HEAD;
        parent::init();
    }
}
