<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

use yii\web\View;

// use yii\web\View;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //        'bootstrap/dist/css/bootstrap.css',
        //        '//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css',
        //        'https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css',
        //        'https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css'
        "AdminLTE-3.0.0-alpha.2/plugins/font-awesome/css/font-awesome.min.css",
        // "AdminLTE-3.0.0-alpha.2/plugins/icofont/icofont.min.css",
        "AdminLTE-3.0.0-alpha.2/plugins/ionicons/css/ionicons.min.css",
        "AdminLTE-3.0.0-alpha.2/dist/css/adminlte.min.css",
        // "AdminLTE-3.0.0-alpha.2/plugins/iCheck/flat/blue.css",
        // "AdminLTE-3.0.0-alpha.2/plugins/morris/morris.css",
        // "AdminLTE-3.0.0-alpha.2/plugins/jvectormap/jquery-jvectormap-1.2.2.css",
        "AdminLTE-3.0.0-alpha.2/plugins/datepicker/datepicker3.css",
        "AdminLTE-3.0.0-alpha.2/plugins/daterangepicker/daterangepicker-bs3.css",
        // "AdminLTE-3.0.0-alpha.2/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css",
        "AdminLTE-3.0.0-alpha.2/plugins/select2/select2.min.css",
        "AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.css",
        // "AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.css",
        //        "AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables_themeroller.css",
        "css/site.css",
    ];
    public $js = [
        //        'js/jquery.js',
        //        'bootstrap/dist/js/bootstrap.js',
        //        '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'
         "AdminLTE-3.0.0-alpha.2/plugins/jquery/jquery.min.js",
         "AdminLTE-3.0.0-alpha.2/plugins/jquery/jquery-ui.min.js",
        "AdminLTE-3.0.0-alpha.2/plugins/bootstrap/js/bootstrap.bundle.min.js",
         "AdminLTE-3.0.0-alpha.2/plugins/raphael/raphael-min.js",
         "AdminLTE-3.0.0-alpha.2/plugins/morris/morris.min.js",
         "AdminLTE-3.0.0-alpha.2/plugins/sparkline/jquery.sparkline.min.js",
         "AdminLTE-3.0.0-alpha.2/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
         "AdminLTE-3.0.0-alpha.2/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
         "AdminLTE-3.0.0-alpha.2/plugins/knob/jquery.knob.js",
         "AdminLTE-3.0.0-alpha.2/plugins/moment/moment.min.js",
        "AdminLTE-3.0.0-alpha.2/plugins/daterangepicker/daterangepicker.js",
        "AdminLTE-3.0.0-alpha.2/plugins/datepicker/bootstrap-datepicker.js",
        // "AdminLTE-3.0.0-alpha.2/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js",
        "AdminLTE-3.0.0-alpha.2/plugins/slimScroll/jquery.slimscroll.min.js",
        "AdminLTE-3.0.0-alpha.2/plugins/fastclick/fastclick.js",
        "AdminLTE-3.0.0-alpha.2/plugins/select2/select2.full.min.js",
        "AdminLTE-3.0.0-alpha.2/dist/js/adminlte.js",
        //        "AdminLTE-3.0.0-alpha.2/dist/js/pages/dashboard.js",
        "AdminLTE-3.0.0-alpha.2/dist/js/demo.js",
        "AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.min.js",
        "AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.js",
        // "AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.js",
        "AdminLTE-3.0.0-alpha.2/plugins/jquerynumber/jquery.number.min.js",
        "AdminLTE-3.0.0-alpha.2/plugins/timepicker/bootstrap-timepicker.js",

        "js/sweetalert.min.js",
        "js/custom.js",
        "js/jquery.number.js",
        "js/jquery.inputmask.bundle.js",
    ];

    public $depends = [
//        'yii\web\YiiAsset',

// 'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        $this->jsOptions['position'] = View::POS_HEAD;
        parent::init();
    }
}
