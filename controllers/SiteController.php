<?php

namespace app\controllers;

use app\models\Artikel;
use app\models\ArtikelDetail;
use app\models\Member;
use app\models\TiketBooking;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    function actionFronts()
    {
        $this->layout = "@app/views/layouts/main-front";
        $getarticle = (new Query())->from('artikel')->all();
        $data['artikel'] = $getarticle;
        return $this->render('front', $data);
    }

    function actionGetlisttiket()
    {

    }

    function actionArtikelbyr($id = null)
    {

        $getdetail = ArtikelDetail::find()->where(['id_artikel' => $id])->andWhere(['>', 'jml_tiket', 'jml_terjual'])->all();
        if (!empty($getdetail)):
            $getartikel = Artikel::find()->where(['id' => $id])->andWhere(['>=', 'valid_until', date('Y-m-d')])->one();
            if(!empty($getartikel)){
                $parsing=array();
                foreach($getdetail as $r){
                    $sesi = strtolower(str_replace(' ','',$r['sesi']));
                    $jam_tayang = str_replace(':','',$r['jam_tayang']);

                    if(!isset($parsing[$sesi.$jam_tayang])){
                        $parsing[$sesi.$jam_tayang]['sesi']=$r['sesi'];
                        $parsing[$sesi.$jam_tayang]['jam_tayang']=date("H:i",strtotime($r['jam_tayang']));
                        $parsing[$sesi.$jam_tayang][]=array(
                            'id'=>$r['id'],
                            'id_artikel'=>$r['id_artikel'],
                            'jml_tiket'=>$r['jml_tiket'],
                            'ket'=>$r['ket'],
                            'jml_terjual'=>$r['jml_terjual'],
                            'harga'=>$r['harga'],
                        );
                    }else{
                        $parsing[$sesi.$jam_tayang]['sesi']=$r['sesi'];
                        $parsing[$sesi.$jam_tayang]['jam_tayang']=date("H:i",strtotime($r['jam_tayang']));
                        $parsing[$sesi.$jam_tayang][]=array(
                            'id'=>$r['id'],
                            'id_artikel'=>$r['id_artikel'],
                            'jml_tiket'=>$r['jml_tiket'],
                            'ket'=>$r['ket'],
                            'jml_terjual'=>$r['jml_terjual'],
                            'harga'=>$r['harga'],
                            'sesi'=>$r['sesi'],
                            'jam_tayang'=>$r['jam_tayang'],
                        );
                    }
                }
                $data['artikel'] = $getartikel;
                $data['artikel_detail'] = $parsing;
                return $this->renderPartial('modalbuy', $data);
            }else{
                echo " Data Tiket Habis / Tidak Tersedia.";
                exit;
            }
        else:
            echo " Data Tiket Habis / Tidak Tersedia.";
            exit;
        endif;
    }

    function actionBuytiket()
    {
        if ($_POST) {
            $req = Yii::$app->request->post();
            if(!isset($req['detailprod'])){
                echo "ada kesalahan sistem";exit;
            }
            $iddetail = $req['detailprod'];
            $idproduk = $req['idproduk'];
            $jmlbook = $req['jmlbook'];
            $namapesan = $req['namapesan'];
            $tempatlahir = $req['tempatlahir'];
            $tgl_lahir = $req['tgl_lahir'];
            $domisili = $req['domisili'];
            $no_telp = $req['no_telp'];
            $email = $req['email'];
            if (!$this->validateEmail($email)) {
                echo "email not valid";
                exit;
            } else {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $harga = ArtikelDetail::find()
                        ->where(['id' => $iddetail])
                        ->andWhere(['>', 'jml_tiket', 'jml_terjual'])
                        ->one();
                    $findmember = Member::find()->where(['email' => $email])->orWhere(['phone' => $no_telp])->one();
                    if (!empty($harga)):
                        $idmember = uniqid();
                        if (!empty($findmember)) {
                            $idmember = $findmember->id;
                        } else {
                            $member = new Member();
                            $member->id = $idmember;
                            $member->fullname = $namapesan;
                            $member->email = $email;
                            $member->phone = $no_telp;
                            $member->tgl_lahir = $tgl_lahir;
                            $member->tempat_lahir = $tempatlahir;
                            $member->domisili = $domisili;
                            $member->created_by = 'GUEST';
                            $member->created_at = date("Y-m-d H:i:s");
                            $member->updated_at = date("Y-m-d H:i:s");
                            if (!$member->save()) {
                                echo "Error Member";
                                print_r($member->errors);
                            }
                        }
                        $tiketbook = new TiketBooking();
                        $orderno = rand(0,999999999);
                        $getdisokon = 0;//$this->hitungDiskon($jmlbook);
                        $total = ($getdisokon > 0)?($jmlbook * $harga['harga']) - (($jmlbook * $harga['harga'])*($getdisokon/100)):($jmlbook * $harga['harga']);

//                        echo $total." ".$getdisokon;exit;
                        $tiketbook->id_artikel_detail = $iddetail;
                        $tiketbook->id_member = $idmember;
                        $tiketbook->jml_tiket = $jmlbook;
                        $tiketbook->harga = $harga['harga'];
                        $tiketbook->jenis = 'ONLINE';
                        $tiketbook->diskon = (string)$getdisokon;
                        $tiketbook->total_harga = (string)$total ;
                        $tiketbook->status = $total == 0 ?"konfirmasi":'waiting';
                        $tiketbook->no_order = (string)$orderno;
                        $tiketbook->created_by = date("Y-m-d H:i:s");
                        $tiketbook->updated_by = date("Y-m-d H:i:s");
                        if (!$tiketbook->save()) {
                            echo "Error Tiket Book";
                            print_r($tiketbook->errors);
                        }
                        $harga->jml_terjual = $harga->jml_terjual + $jmlbook;
                        $harga->updated_at = date("Y-m-d H:i:s");
                        if (!$harga->save()) {
                            echo "Error Harga";
                            print_r($harga->errors);
                        }

                        $total = $harga->harga * $jmlbook;
                        $this->kirimEmail($email, $namapesan, number_format($total, 0, '', '.'),$orderno);
                    endif;
                    $transaction->commit();
                } catch (Exception $e) {
                    $transaction->rollback();
                }
            }

            return $this->redirect(Url::to(['/info/index','order'=>$orderno]));
        }
        return $this->redirect(Url::to(['/']));
    }
    function hitungDiskon($jml){
        $diskon = 0;
        if(date("Y-m-d") <= "2019-01-15") {
            if ($jml >= 20) {
                $diskon = 20;
            } elseif ($jml >= 10 && $jml < 20) {
                $diskon = 16;
            } elseif ($jml >= 5 && $jml < 10) {
                $diskon = 13.6;
            } elseif ($jml >= 2 && $jml < 5) {
                $diskon = 13.6;
            } else {
                $diskon = 0;
            }
        }
        return $diskon;
    }

    function actionSendemail()
    {
        $a = Yii::$app->mailer->compose()
            ->setFrom('noreply@sensyari-indonesia.com')
            ->setTo('fahmi.hilmansyah@gmail.com')
            ->setSubject('Message subject')
            ->setTextBody('Plain text content')
            ->setHtmlBody('<b>HTML content</b>')
            ->send();

        echo "sds";
        exit;
    }

    function validateEmail($email = '')
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }

    function kirimEmail($to, $nama, $jmlduit,$order)
    {
//        $message_html = '<p>Hallo <b>{{nama}}</b>,</p>
//<p>Barusan anda melakukan Booking, silakan transfer <b>{{jml_duit}}</b> dan kirim kan bukti agar kami segera proses.</p>';
//        $message_html = str_replace(array('{{nama}}', '{{jml_duit}}'), array($nama, $jmlduit), $message_html);
        $tiketbook = (new Query())
            ->select([new Expression('sum(jml_tiket * harga) total_tagihan'),'status','no_order','diskon','total_harga'])
            ->from('tiket_booking')->where(['no_order'=>$order])->one();
        $data['tiket']=$tiketbook;
        $this->layout=false;
        $message_html = $this->render('@app/views/info/index',$data);
        $a = Yii::$app->mailer->compose()
            ->setFrom('sensyari@sensyari-indonesia.com')
            ->setTo($to)
            ->setSubject('[Transfer] Silakan Transfer')
//                ->setTextBody('Plain text content')
            ->setHtmlBody($message_html)
            ->send();
    }
}
