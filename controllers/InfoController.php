<?php

namespace app\controllers;

use app\models\Artikel;
use app\models\ArtikelDetail;
use app\models\Member;
use app\models\TiketBooking;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class InfoController extends Controller
{
    public $layout = false;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($order='')
    {
        $tiketbook = (new Query())
            ->select([new Expression('sum(jml_tiket * harga) total_tagihan'),'status','no_order','diskon','total_harga'])
            ->from('tiket_booking')->where(['no_order'=>$order])->one();
        $data['tiket']=$tiketbook;
        return $this->render('index',$data);
    }
    function actionTransaksi(){

    }
}
