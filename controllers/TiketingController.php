<?php

namespace app\controllers;

use Yii;
use app\models\TiketBooking;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TiketingController implements the CRUD actions for TiketBooking model.
 */
class TiketingController extends Controller
{
    public $layout = 'main-adm';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create','artikel','createdet','updatedet','getdataartikel','getdatadetartikel'],
                'rules' => [
                    [
                        'actions' => ['index', 'create','view','delete','update','artikel','createdet','updatedet','getdataartikel','getdatadetartikel'],
                        'allow' => true,
                        //'roles' => ['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->identity['role'] == '1'
                            );
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all TiketBooking models.
     * @return mixed
     */
    public function actionIndex()
    {
//        $this->layout = "@app/views/layouts/main-adm";
        $dataProvider = new ActiveDataProvider([
            'query' => (new Query())
                ->select([
                    'tiket_booking.*',
                    'member.fullname',
                    'member.email',
                    'member.phone',
                    'member.tgl_lahir',
                    'member.tempat_lahir',
                    'member.domisili',
                    'artdata.judul',
                    'artdata.sesi',
                    'artdata.jam_tayang',
                    'artdata.ket',
                ])
                ->from('tiket_booking')
                ->join('join','member','member.id=tiket_booking.id_member')
                ->join('join','(select artikel.judul, artikel_detail.id, artikel_detail.ket, artikel_detail.jml_tiket, artikel_detail.jml_terjual, artikel_detail.harga, artikel_detail.sesi, artikel_detail.jam_tayang from artikel_detail join artikel on artikel.id = artikel_detail.id_artikel) artdata', 'artdata.id = tiket_booking.id_artikel_detail')
            ->orderBy(['tiket_booking.created_by'=>SORT_DESC])
            //TiketBooking::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TiketBooking model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TiketBooking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TiketBooking();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TiketBooking model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TiketBooking model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TiketBooking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TiketBooking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TiketBooking::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    function actionProses(){
        if($_POST){
            $req = Yii::$app->request->post();
            $pros = $req['reqproses'];
            $idtiket = $req['idtiket'];
            $findtiket = (new Query())
                ->select(['tiket_booking.*','member.fullname','member.email','member.phone','member.tgl_lahir','member.tempat_lahir','member.domisili'])
                ->from('tiket_booking')
                ->join('join','member','member.id = tiket_booking.id_member')
                ->where(['tiket_booking.id'=>$idtiket])->one();
//            $findtiket = TiketBooking::find()->where(['id'=>$idtiket])->one();
            if(!empty($findtiket)) {
                $username = Yii::$app->user->identity->username;
                if ($pros == 'cancel') {
                    Yii::$app->db->createCommand()->update('tiket_booking',array('status'=>'cancel','updated_by'=>date("Y-m-d H:i:s"),'proses_by'=>$username),array('no_order'=>$findtiket['no_order'],'id'=>$findtiket['id']))->execute();
                    $this->kirimEmail($findtiket['email'], $findtiket['fullname'], number_format($findtiket['total_harga'], 0, '', '.'),$findtiket['no_order']);
                    echo json_encode(array('rc'=>200,'msg'=>'Sudah di proses dan sistem telah mengirim E-Mail'));
                    exit;
                }
                if ($pros == 'approve') {
                    Yii::$app->db->createCommand()->update('tiket_booking',array('status'=>'konfirmasi','updated_by'=>date("Y-m-d H:i:s"),'proses_by'=>$username),array('no_order'=>$findtiket['no_order'],'id'=>$findtiket['id']))->execute();
                    $this->kirimEmail($findtiket['email'], $findtiket['fullname'], number_format($findtiket['total_harga'], 0, '', '.'),$findtiket['no_order']);
                    echo json_encode(array('rc'=>200,'msg'=>'Sudah di proses dan sistem telah mengirim E-Mail'));
                    exit;}
            }
            echo json_encode(array('rc'=>400,'msg'=>'Gagal di proses'));
            exit;
        }
    }
    function kirimEmail($to, $nama, $jmlduit,$order)
    {
//        $message_html = '<p>Hallo <b>{{nama}}</b>,</p>
//<p>Barusan anda melakukan Booking, silakan transfer <b>{{jml_duit}}</b> dan kirim kan bukti agar kami segera proses.</p>';
//        $message_html = str_replace(array('{{nama}}', '{{jml_duit}}'), array($nama, $jmlduit), $message_html);
        $tiketbook = (new Query())
            ->select([new Expression('sum(jml_tiket * harga) total_tagihan'),'status','no_order','diskon','total_harga'])
            ->from('tiket_booking')->where(['no_order'=>$order])->one();
        $data['tiket']=$tiketbook;
        $this->layout=false;
        $message_html = $this->render('@app/views/info/index',$data);
        $a = Yii::$app->mailer->compose()
//            ->setFrom('noreply@sensyari-indonesia.com')
            ->setFrom('sensyari@sensyari-indonesia.com')
            ->setTo($to)
            ->setSubject('[Informasi Order]')
//                ->setTextBody('Plain text content')
            ->setHtmlBody($message_html)
            ->send();
    }
}
