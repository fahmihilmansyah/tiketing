<?php

namespace app\controllers;

use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Users;

class UsersController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
//        print_r(Yii::$app->user->identity->username);exit;
        $this->layout = "@app/views/layouts/main-adm";
        return $this->render('index');
    }

    public function actionCreate()
    {
        // var_dump($_POST);exit();
        $model = new Users();
        if ($model->load(Yii::$app->request->post())) {
                $model->username = $_POST['Users']['username'];
                $model->password = sha1($_POST['Users']['password']);
                $model->email = $_POST['Users']['email'];
                $model->role = $_POST['Users']['role'];
                $model->created_at = date('Y-m-d H:i:s');
                if ($model->save()) {
                    $hasil = array(
                        'status' => "success",
                        'header' => "Berhasil",
                        'message' => "Users Berhasil Di Tambahkan !",
                    );
                    echo json_encode($hasil);
                    die();
                }
        }

        return $this->renderPartial('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->username = $_POST['Users']['username'];
            $model->password = sha1($_POST['Users']['password']);
            $model->email = $_POST['Users']['email'];
            $model->role = $_POST['Users']['role'];
            $model->updated_at = date('Y-m-d H:i:s');
            if ($model->save()) {
                $hasil = array(
                    'status' => "success",
                    'header' => "Berhasil",
                    'message' => "Users Berhasil Di Update !",
                );
                echo json_encode($hasil);
                die();
            }
        }

        return $this->renderPartial('update', [
            'model' => $model,
            'id' => $id,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $hasil = array(
            'status' => "success",
            'header' => "Berhasil",
            'message' => "Users Berhasil Di Hapus !",
        );
        echo json_encode($hasil);
        die();
        // return $this->redirect(['adm/artikel']);
    }

    public function actionGetdatausers()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Users::find()->asArray()->all();

        $row = array();
        $i = 0;
        foreach ($data as $idx => $value) {
            //Asiign All Value To Row
            foreach ($value as $key => $val) {
                $row[$i][$key] = $val;
            }
            $row[$i]['username'] = $value['username'];
            $row[$i]['email'] = $value['email'];
            $row[$i]['role'] = $value['role'];
            $row[$i]['fungsi'] = "
                <button onclick='updateusers(\"" . $value['id'] . "\")' type='button' rel='tooltip' data-toggle='tooltip' title='Edit Users' class='btn btn-sm btn-warning'><i class='fa fa-edit'></i></button>
                <button onclick='deleteusers(\"" . $value['id'] . "\")' type='button' rel='tooltip' data-toggle='tooltip' title='Hapus Users' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></button>
            ";
            $i++;
        }
        $hasil['data'] = $row;
            // var_dump($hasil);exit();
        return $hasil;
    }

    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }


}
