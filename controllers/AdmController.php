<?php

namespace app\controllers;

use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Artikel;
use app\models\ArtikelDetail;

class AdmController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public $layout = 'main-adm';
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create','artikel','createdet','updatedet','getdataartikel','getdatadetartikel'],
                'rules' => [
                    [
                        'actions' => ['index', 'create','view','delete','update','artikel','createdet','updatedet','getdataartikel','getdatadetartikel'],
                        'allow' => true,
                        //'roles' => ['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->identity['role'] == '1'
                            );
                        }
                    ],
                ],
            ],
        ];
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['logout'],
//                'rules' => [
//                    [
//                        'actions' => ['logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//            // 'verbs' => [
//            //     'class' => VerbFilter::className(),
//            //     'actions' => [
//            //         'logout' => ['post'],
//            //     ],
//            // ],
//        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        // $this->layout = "@app/views/layouts/main-adm";
        // return $this->render('index');
//        if (Yii::$app->user->isGuest)
//            return $this->redirect(['adm/login']);

        return $this->redirect(['adm/home']);
    }

    public function actionHome()
    {
        // $role = Yii::$app->user->identity->role;
        // $type = Yii::$app->user->identity->type;
        // if($type == "User Eksekutif")
        // {
        //     return $this->redirect('../dashboard/cockpit');
        // }
        // else
        // {
        //     if($role == "UnitKerja" || $role == "UnitBisnis" || $role == "Direktorat") {
        //         return $this->redirect('../dashboard/landingpage');
        //     } elseif($role == "UnitBa") {
        //         return $this->redirect('../monitoring/default/juskeb');
        //     } else {
        //         return $this->redirect('../dashboard');
        //     }
        // }
        // $this->layout = true;
        return $this->render('home');
    }

    public function actionCreate()
    {
        $model = new Artikel();
        // $file = isset($_FILES['file_items']) ? $_FILES['file_items'] : null;
        if ($model->load(Yii::$app->request->post())) {
            // if(!is_null($file) || $file['error'] == 0 )
            // {
                // $RootTmp = \Yii::getAlias('@app') . '/web/uploads/img_artikel/';
                // if (!is_dir($RootTmp)) { mkdir($RootTmp);}
                // chmod($RootTmp, 0777);
                //
                // $path     = $file['name'];
                // $dateRand = date('YmdHis',time())."-".mt_rand();
                // $file_name= $dateRand."_".$path;
                // $newpath  = \Yii::getAlias('@app') . '/web/uploads/img_artikel/'.$path;
                // move_uploaded_file($file['tmp_name'], $newpath);

                $model->judul = $_POST['Artikel']['judul'];
                $model->desc = $_POST['Artikel']['desc'];
                $model->gambar = $_POST['Artikel']['gambar'];
                $model->mini_desc = $_POST['Artikel']['mini_desc'];
                $model->tgl_event = $_POST['Artikel']['tgl_event'];
                $model->valid_until = $_POST['Artikel']['valid_until'];
                $model->created_by = Yii::$app->user->identity->username;
                $model->created_at = date('Y-m-d H:i:s');
                if ($model->save()) {
                    $hasil = array(
                        'status' => "success",
                        'header' => "Berhasil",
                        'message' => "Artikel Berhasil Di Tambahkan !",
                    );
                    echo json_encode($hasil);
                    die();
                }
            // }
        }

        return $this->renderPartial('create', [
            'model' => $model,
        ]);
    }

    public function actionCreatedet($id)
    {
        $model = new ArtikelDetail();
        $model2 = Artikel::find()->where(['id' => $id])->asArray()->one();
        // $file = isset($_FILES['file_items']) ? $_FILES['file_items'] : null;
        if ($model->load(Yii::$app->request->post())) {
                $harga = $_POST['ArtikelDetail']['harga'];
                $r_harga = str_replace(',','',$harga);
                // var_dump($r_harga);exit();
            // if(!is_null($file) || $file['error'] == 0 )
            // {
                // $RootTmp = \Yii::getAlias('@app') . '/web/uploads/img_artikel/';
                // if (!is_dir($RootTmp)) { mkdir($RootTmp);}
                // chmod($RootTmp, 0777);
                //
                // $path     = $file['name'];
                // $dateRand = date('YmdHis',time())."-".mt_rand();
                // $file_name= $dateRand."_".$path;
                // $newpath  = \Yii::getAlias('@app') . '/web/uploads/img_artikel/'.$path;
                // move_uploaded_file($file['tmp_name'], $newpath);

                $model->id_artikel = $id;
                $model->ket = $_POST['ArtikelDetail']['ket'];
                $model->jml_tiket = $_POST['ArtikelDetail']['jml_tiket'];
                $model->jml_terjual = $_POST['ArtikelDetail']['jml_terjual'];
                $model->harga = (string) $r_harga;
                $model->sesi = $_POST['ArtikelDetail']['sesi'];
                $model->jam_tayang = $_POST['ArtikelDetail']['jam_tayang'];
                $model->created_by = Yii::$app->user->identity->username;
                $model->created_at = date('Y-m-d H:i:s');
                if ($model->save()) {
                    $hasil = array(
                        'status' => "success",
                        'header' => "Berhasil",
                        'message' => "Detail Artikel Berhasil Di Tambahkan !",
                    );
                    echo json_encode($hasil);
                    die();
                }
            // }
        }

        return $this->renderPartial('create_det', [
            'model' => $model,
            'model2' => $model2,
            'id' => $id
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->judul = $_POST['Artikel']['judul'];
            $model->desc = $_POST['Artikel']['desc'];
            $model->gambar = $_POST['Artikel']['gambar'];
            $model->mini_desc = $_POST['Artikel']['mini_desc'];
            $model->tgl_event = $_POST['Artikel']['tgl_event'];
            $model->valid_until = $_POST['Artikel']['valid_until'];
            $model->created_by = Yii::$app->user->identity->username;
            $model->updated_at = date('Y-m-d H:i:s');
            if ($model->save()) {
                $hasil = array(
                    'status' => "success",
                    'header' => "Berhasil",
                    'message' => "Artikel Berhasil Di Update !",
                );
                echo json_encode($hasil);
                die();
            }
        }

        return $this->renderPartial('update', [
            'model' => $model,
            'id' => $id,
        ]);
    }

    public function actionUpdatedet($id)
    {
        $model = $this->findModel2($id);
        $model2 = Artikel::find()->where(['id' => $model->id_artikel])->asArray()->one();
        // var_dump($model2);exit();
        if ($model->load(Yii::$app->request->post())) {
            $harga = $_POST['ArtikelDetail']['harga'];
            $r_harga = str_replace(',','',$harga);

            $model->ket = $_POST['ArtikelDetail']['ket'];
            $model->jml_tiket = $_POST['ArtikelDetail']['jml_tiket'];
            $model->jml_terjual = $_POST['ArtikelDetail']['jml_terjual'];
            $model->harga = (string) $r_harga;
            $model->sesi = $_POST['ArtikelDetail']['sesi'];
            $model->jam_tayang = $_POST['ArtikelDetail']['jam_tayang'];
            $model->updated_by = Yii::$app->user->identity->username;
            $model->updated_at = date('Y-m-d H:i:s');
            if ($model->save()) {
                $hasil = array(
                    'status' => "success",
                    'header' => "Berhasil",
                    'message' => "Detail Artikel Berhasil Di Update !",
                );
                echo json_encode($hasil);
                die();
            }
        }

        return $this->renderPartial('update_det', [
            'model' => $model,
            'model2' => $model2,
            'id' => $id
        ]);
    }

    public function actionDetartikel($id)
    {
        // $model = $this->findModel($id);
        $modelArtikel = Artikel::find()->where(['id' => $id])->asArray()->one();

        return $this->renderPartial('detailartikel', [
            'modelArtikel' => $modelArtikel,
            // 'id' => $id,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $hasil = array(
            'status' => "success",
            'header' => "Berhasil",
            'message' => "Artikel Berhasil Di Hapus !",
        );
        echo json_encode($hasil);
        die();
        // return $this->redirect(['adm/artikel']);
    }

    public function actionDeletedet($id)
    {
        $this->findModel2($id)->delete();
        $hasil = array(
            'status' => "success",
            'header' => "Berhasil",
            'message' => "Detail Artikel Berhasil Di Hapus !",
        );
        echo json_encode($hasil);
        die();
        // return $this->redirect(['adm/artikel']);
    }

    public function actionArtikel()
    {
        // $this->layout = "@app/views/layouts/main-adm";
        return $this->render('artikel');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = "@app/views/layouts/main-login";
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            // var_dump($_POST);exit();
            return $this->redirect(['adm/artikel']);
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    function actionFronts(){
        $this->layout = "@app/views/layouts/main-front";
        $getarticle = (new Query())->from('artikel')->all();
        $data['artikel'] =$getarticle;
        return $this->render('front',$data);
    }
    function actionGetlisttiket(){

    }

    public function actionGetdataartikel()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Artikel::find()
        ->orderBy(['id' => SORT_DESC])
        ->asArray()
        ->all();

        $row = array();
        $i = 0;
        foreach ($data as $idx => $value) {
            //Asiign All Value To Row
            foreach ($value as $key => $val) {
                $row[$i][$key] = $val;
            }
            $row[$i]['id'] = $value['id'];
            $row[$i]['judul'] = $value['judul'];
            $row[$i]['mini_desc'] = $value['mini_desc'];
            // $row[$i]['desc'] = $value['desc'];
            $row[$i]['tgl_event'] = $value['tgl_event'];
            $row[$i]['valid_until'] = $value['valid_until'];
            $row[$i]['fungsi'] = "
                <button onclick='detailartikel(\"" . $value['id'] . "\")' type='button' rel='tooltip' data-toggle='tooltip' title='Detail Artikel' class='btn btn-sm btn-primary'><i class='fa fa-list'></i></button>
                <button onclick='updateartikel(\"" . $value['id'] . "\")' type='button' rel='tooltip' data-toggle='tooltip' title='Edit Artikel' class='btn btn-sm btn-warning'><i class='fa fa-edit'></i></button>
                <button onclick='deleteartikel(\"" . $value['id'] . "\")' type='button' rel='tooltip' data-toggle='tooltip' title='Hapus Artikel' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></button>
            ";
            $i++;
        }
        $hasil['data'] = $row;
            // var_dump($hasil);exit();
        return $hasil;
    }

    public function actionGetdatadetartikel($idartikel=null)
    {
        $data = (new \yii\db\Query())
            ->select(['a.id', 'a.harga', 'a.sesi', 'a.jam_tayang', 'a.ket', 'a.jml_tiket', 'a.jml_terjual', 'b.judul'])
            ->from('artikel_detail a')
            ->join('INNER JOIN', 'artikel b', 'b.id=a.id_artikel')
            ->where(['a.id_artikel' => $idartikel])
            ->orderBy(['a.id' => SORT_DESC])
            ->all();

        $row = array();
        $i = 0;
        foreach ($data as $idx => $value) {
            //Asiign All Value To Row
            foreach ($value as $key => $val) {
                $row[$i][$key] = $val;
            }

            $c_jmltiket = number_format($value['jml_tiket']);
            $c_jmlterjual = number_format($value['jml_terjual']);
            $c_harga = number_format($value['harga'],0,',','.');

            $tayang = $value['jam_tayang'];
            $c_tayang = substr( $tayang,0,5 );

            $row[$i]['id'] = $value['id'];
            $row[$i]['ket'] = $value['ket'];
            $row[$i]['jml_tiket'] = $c_jmltiket;
            $row[$i]['jml_terjual'] = $c_jmlterjual;
            $row[$i]['harga'] = $c_harga;
            $row[$i]['sesi'] = $value['sesi'];
            $row[$i]['jam_tayang'] = $c_tayang;
            $row[$i]['fungsi'] = "
                <button onclick='updatedetartikel(\"" . $value['id'] . "\")' type='button' rel='tooltip' data-toggle='tooltip' title='Edit Artikel' class='btn btn-sm btn-warning'><i class='fa fa-edit'></i></button>
                <button onclick='deletedetartikel(\"" . $value['id'] . "\")' type='button' rel='tooltip' data-toggle='tooltip' title='Hapus Artikel' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></button>
            ";
            $i++;
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $hasil['data'] = $row;

            // var_dump($hasil);exit();
        return $hasil;

    }
    public function actionLoadtbl($idartikel) {
        return $this->renderPartial('_viewtbl',['idartikel' => $idartikel]);
    }
    protected function findModel($id)
    {
        if (($model = Artikel::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
    protected function findModel2($id)
    {
        if (($model = ArtikelDetail::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
