<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "artikel_detail".
 *
 * @property int $id
 * @property int $id_artikel
 * @property string $ket untuk sesi
 * @property string $jml_tiket
 * @property string $jml_terjual
 * @property string $harga
 * @property string $sesi
 * @property string $jam_tayang
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 */
class ArtikelDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'artikel_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_artikel'], 'integer'],
            [['jml_tiket', 'jml_terjual', 'harga'], 'number'],
            [['jam_tayang', 'created_at', 'updated_at'], 'safe'],
            [['ket', 'sesi'], 'string', 'max' => 255],
            [['created_by', 'updated_by'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_artikel' => 'Id Artikel',
            'ket' => 'Ket',
            'jml_tiket' => 'Jml Tiket',
            'jml_terjual' => 'Jml Terjual',
            'harga' => 'Harga',
            'sesi' => 'Sesi',
            'jam_tayang' => 'Jam Tayang',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
