<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tiket_booking".
 *
 * @property int $id
 * @property string $id_member
 * @property int $id_artikel_detail
 * @property string $jml_tiket
 * @property string $status konfirmasi,menunggu,terbayar,batal
 * @property string $created_by
 * @property string $updated_by
 * @property string $jenis jenis tiket offline online
 * @property string $harga
 * @property string $no_order
 * @property string $diskon
 * @property string $total_harga
 */
class TiketBooking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tiket_booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'id_artikel_detail'], 'integer'],
            [['jml_tiket', 'harga'], 'number'],
            [['created_by', 'updated_by'], 'safe'],
            [['diskon','total_harga','no_order','id_member','status', 'jenis'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_member' => 'Id Member',
            'id_artikel_detail' => 'Id Artikel Detail',
            'jml_tiket' => 'Jml Tiket',
            'status' => 'Status',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'jenis' => 'Jenis',
            'harga' => 'Harga',
        ];
    }
}
