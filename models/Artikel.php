<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "artikel".
 *
 * @property int $id
 * @property string $judul
 * @property string $desc
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $gambar
 * @property string $mini_desc
 * @property string $tgl_event
 * @property string $valid_until
 * @property string $is_tiket
 */
class Artikel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'artikel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['desc'], 'string'],
            [['created_at', 'updated_at', 'tgl_event', 'valid_until'], 'safe'],
            [['judul', 'created_by', 'mini_desc','is_tiket'], 'string', 'max' => 255],
            [['gambar'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'desc' => 'Deskripsi',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'gambar' => 'URL Gambar',
            'mini_desc' => 'Mini Deskripsi',
            'tgl_event' => 'Tgl Event',
            'valid_until' => 'Valid Until',
        ];
    }
}
