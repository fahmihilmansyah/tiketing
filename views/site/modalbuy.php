<?php
/**
 * Created by PhpStorm.
 * Project : tiketing
 * User: fahmihilmansyah
 * Date: 2018-12-20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 11:12
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
?>
<div class="row mb-15">
    <div class="col-md-5">
        <img src="<?php echo $artikel['gambar'] ?>" class="img-content"/>
    </div>
    <div class="col-md-7">
        <div class="desc-content">
            <h6 class="card-title mt-0 mb-5"><?php echo $artikel['judul'] ?></h6>
            <p class="lh-n">
                <?php echo $artikel['mini_desc'] ?>
            </p>
        </div>
    </div>
</div>
<hr>
<?php echo \yii\helpers\Html::beginForm(\yii\helpers\Url::to(['/site/buytiket'], true), 'POST'); ?>

<ul class="nav nav-pills oneline" id="myTab" role="tablist">
    <?php foreach ($artikel_detail as $k => $v): ?>
        <li class="nav-item">
            <a class="nav-link" id="<?php echo $k ?>-tab" data-toggle="tab" href="#<?php echo $k ?>" role="tab"
               aria-controls="<?php echo $k ?> aria-selected="
               true"><?php echo $v['sesi'] . " " . $v['jam_tayang'] ?></a>
        </li>
    <?php endforeach; ?>
</ul>
<div class="tab-content" id="myTabContent">
    <?php foreach ($artikel_detail as $k => $v): ?>
        <div class="tab-pane fade" id="<?php echo $k ?>" role="tabpanel" aria-labelledby="profile-tab">
            <div class="form-group">
                <?php
                foreach ($v as $kr => $vr):
                    if (is_array($vr)):
                        $i = 1;
                        ?>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input required type="radio" id="customRadioInline<?php echo $vr['id'] ?>" name="detailprod"
                                   value="<?php echo $vr['id'] ?>" class="custom-control-input">
                            <label class="custom-control-label"
                                   for="customRadioInline<?php echo $vr['id'] ?>"><?php echo $vr['ket'] . " (" . number_format($vr['harga'], 0, '', '.') . ")" ?></label>
                        </div>
                        <?php $i++;
                    endif;
                endforeach; ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<input type="hidden" name="idproduk" value="<?php echo $_GET['id'] ?>">
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Jumlah Booking</label>
            <input type="number" name="jmlbook" required class="form-control" value="1"
                   placeholder="Tulis Jumlah Booking Anda">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="namapesan" required class="form-control" placeholder="Tulis Nama Anda">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Tempat Lahir</label>
            <input type="text" name="tempatlahir" required class="form-control" placeholder="Tulis Tempat Lahir Anda">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Tanggal Lahir</label>
            <input type="date" name="tgl_lahir" required class="form-control" placeholder="">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Tlp / HP</label>
            <input type="text" name="no_telp" required class="form-control" placeholder="Tulis Nomor Tlp / HP Anda">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" required class="form-control" placeholder="Tulis Email Anda">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Domisili</label>
            <input type="text" name="domisili" required class="form-control" placeholder="Tulis Domisili Anda">
        </div>
    </div>
</div>
<button class="btn btn-primary"><?php echo $artikel['is_tiket'] == '1' ?'<i class="fa fa-check"></i> Beli Sekarang':'<i class="fa fa-check"></i> Registrasi Sekarang' ?></button>
<?php \yii\helpers\Html::endForm(); ?>
