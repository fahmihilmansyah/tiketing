<!-- =============== START OF HOW IT WORKS SECTION =============== -->
<section class="how-it-works3 ptb100" id="belitiketnya">
    <div class="container">

        <!-- Start of row -->
        <div class="row justify-content-center">
            <div class="col-md-7 text-center">
                <h2 class="title" style="padding-bottom: 0px;">Beli tiketnya </h2>
                <hr>
            </div>
        </div>
        <!-- End of row -->

        <!-- Start of row -->

        <div id="daftarmain">
            <div class="row justify-content-center">
                <?php foreach ($artikel as $r): ?>
                    <div class="col-md-3">
                        <div class="card hoverable" testmasuk="masuk">
                            <div class="img-card" style="background-image:url(<?php echo $r['gambar'] ?>)"></div>
                            <div class="card-body small-pad card-artikel">
                                <h6 class="card-title mt-0 mb-5"><?php echo $r['judul'] ?></h6>
                                <p class="text-small text-muted lh-n"><?php echo $r['mini_desc'] ?></p>
                                <div class="text-center">
                                    <span data-url="<?php  echo  str_replace('http://','https://',\yii\helpers\Url::to(['/site/artikelbyr','id'=>$r['id']],true))?>" href="#" class="btn btn-primary buytiketing"><?php echo $r['is_tiket'] == '1'? '<span class="fa fa-shopping-cart"></span> &nbsp; Beli Tiket':'<span class="fa fa-user"></span> &nbsp; Registrasi'?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="beli" tabindex="-1" role="dialog" aria-labelledby="beliLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="beliLabel">Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modaldata">

            </div>
        </div>
    </div>
</div>

<!-- =============== END OF HOW IT WORKS SECTION =============== -->

<!-- =============== START OF LATEST RELEASES SECTION =============== -->
<script>
    $(".buytiketing").on('click',function () {
        let attrurl = $(this).attr('data-url');
        // $("#beli").modal().load()
        $("#modaldata").empty();
        $('#beli').modal('show').find('.modal-body').load(attrurl);
    })

</script>