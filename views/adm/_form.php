<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>
<style type="text/css">
    .geserkanan {
        margin-right: 5px;
    }
</style>
<div class="box box-primary">
    <div class="box-body">
        <div class="artikel-form">

            <?php $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-4',
                        'wrapper' => 'col-sm-7',
                        // 'label' => 'col-md-4 col-sm-4 col-xs-12',
                        // 'wrapper' => 'col-md-6 col-sm-6 col-xs-12',
                    ],
                ],
                "options" => [
                    "id" => "form-artikel",
                    "class" => "",
                    "data-parsley-validate" => "",
                    'onsubmit'=>'return false;',
                    'enctype'=>'multipart/form-data',
                    'novalidate' => 'novalidate'
                ]
            ]); ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $form->field($model, 'judul')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'mini_desc')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($model, 'tgl_event')->textInput(['maxlength' => true, 'class' => 'form-control form-tanggal']) ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($model, 'valid_until')->textInput(['maxlength' => true, 'class' => 'form-control form-tanggal']) ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <?= $form->field($model, 'gambar')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'http://savings.gov.pk/wp-content/plugins/ldd-directory-lite/public/images/noimage.png']); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group required">
                        <?= $form->field($model, 'desc')->textarea(['maxlength' => true, 'rows' => '6', 'class' => 'form-control', 'placeholder' => 'Typed your description...']); ?>
                    </div>
                </div>
            </div>
            <?php /* <div class="row">
                <div class="col-md-12">
                    <div class="form-group required">
                                <label class="control-label" for="first-name">File Items</label>
                                <!-- <div> -->
                                    <div class="file-loading">
                                        <input type="file" class="form-control" name="file_items" id="file_items" accept="image/*" required/>
                                    </div>
                                <!-- </div> -->
                    </div>
                </div>
            </div> */ ?>

            <div class="row">
                <div class="col-md-12">
                    <?= Html::a($model->isNewRecord ? '<i class="fa fa-floppy-o" aria-hidden="true"></i> Create' : '<i class="fa fa-pencil" aria-hidden="true"></i> Update', $model->isNewRecord ? 'javascript:saveartikel()' : 'javascript:UpdateArtikel("' . $id . '")', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-success pull-right']) ?>
                    <!-- <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button> -->
                    <?= Html::button('<i class="fa fa-times" aria-hidden="true"></i> Close', ['class' => 'btn btn-danger pull-right geserkanan', 'data-dismiss' => 'modal']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        // $("#file_items").fileinput({
        // previewFileType: "image",
        // browseClass: "btn btn-success",
        // browseLabel: "Pick Image",
        // browseIcon: "<i class=\"fa fa-picture\"></i> ",
        // removeClass: "btn btn-danger",
        // removeLabel: "Remove",
        // removeIcon: "<i class=\"fa fa-trash\"></i> ",
        // uploadClass: "btn btn-info",
        // uploadLabel: "Upload",
        // uploadIcon: "<i class=\"fa fa-upload\"></i> "
        // });
        $('.form-tanggal').datepicker({
            format: 'yyyy-mm-dd',
            // startDate: '<?=date('d-m-Y')?>',
            autoclose: true
        });
    });

    function saveartikel() {
        {
            swal({
                title: "Konfirmasi",
                text: "Tambah Artikel ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((ya) => {
                if (ya) {
                    var _data = new FormData($("#form-artikel")[0]);
                    $.ajax({
                        type: "POST",
                        data: _data,
                        dataType: "json",
                        contentType: false,
                        processData: false,
                        url: "<?=\Yii::$app->getUrlManager()->createUrl(['adm/create'])?>",
                        beforeSend: function () {
                            swal({
                                title: 'Harap Tunggu',
                                text: "Memasukan Artikel Baru",
                                icon: 'info',
                                buttons: {
                                    cancel: false,
                                    confirm: false,
                                },
                                closeOnClickOutside: false,
                                onOpen: function () {
                                    swal.showLoading()
                                },
                                closeOnEsc: false,
                            });
                        },
                        complete: function () {
                            swal.close()
                        },
                        success: function (result) {

                            swal(result.header, result.message, result.status);

                            if (result.status == "success") {
                                window.location = "<?=\Yii::$app->getUrlManager()->createUrl(['adm/artikel'])?>";
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Error!", "Terdapat Kesalahan saat memasukan Menu!", "error");
                        }
                    });
                } else {
                    // swal("Informasi", "Dokumen Tidak Dihapus", "info");
                }
            });
        }
    }

    function UpdateArtikel(id) {
        {
            swal({
                title: "Konfirmasi",
                text: "Ubah Artikel ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((ya) => {
                if (ya) {
                    var _data = new FormData($("#form-artikel")[0]);
                    $.ajax({
                        type: "POST",
                        data: _data,
                        dataType: "json",
                        contentType: false,
                        processData: false,
                        url: "<?=\Yii::$app->getUrlManager()->createUrl(['adm/update'])?>?id=" + id,
                        beforeSend: function () {
                            swal({
                                title: 'Harap Tunggu',
                                text: "Mengubah Artikel Baru",
                                icon: 'info',
                                buttons: {
                                    cancel: false,
                                    confirm: false,
                                },
                                closeOnClickOutside: false,
                                onOpen: function () {
                                    swal.showLoading()
                                },
                                closeOnEsc: false,
                            });
                        },
                        complete: function () {
                            swal.close()
                        },
                        success: function (result) {

                            swal(result.header, result.message, result.status);

                            if (result.status == "success") {
                                window.location = "<?=\Yii::$app->getUrlManager()->createUrl(['adm/artikel'])?>";
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Error!", "Terdapat Kesalahan saat memasukan Menu!", "error");
                        }
                    });
                } else {
                    // swal("Informasi", "Dokumen Tidak Dihapus", "info");
                }
            });
        }
    }
</script>
