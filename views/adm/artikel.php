<?php
/**
 * Created by PhpStorm.
 * Project : tiketing
 * User: fahmihilmansyah
 * Date: 2018-12-18
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 00:09
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
 ?>
 <?php

 use yii\bootstrap\ActiveForm;
 use yii\helpers\Html;

 // use fedemotta\datatables\DataTables;

 /* @var $this yii\web\View */
 /* @var $searchModel app\models\PAsetKategoriSearch */
 /* @var $dataProvider yii\data\ActiveDataProvider */

 $this->title = 'Artikel';
 $this->params['breadcrumbs'][] = $this->title;
 ?>
 <style type="text/css">
     .mright {
         margin-right: 5px;
     }

    .modal-lg {
        max-width: 90%;
    }
    /* .swal-modal {
        z-index: 99999999 !important;
    } */
    #modalDetArtikel {
        z-index: 20000000 !important;
        /* overflow-y: hidden; */
    }
    #modalArtikel {
        z-index: 10000000 !important;
        /* overflow-y: hidden; */
    }

    .modal-body {
        overflow-y: hidden !important;
    }

    /* Ensure that the demo table scrolls */
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }

    div.container {
        width: 80%;
    }
 </style>
 <div class="menuartikel-index">

     <h1 class="title"><?= Html::encode($this->title) ?></h1>

     <!-- <p class="pull-right"> -->
     <div class="row">
         <div class="col-md-12" align="right" style="margin-bottom:5px;">
             <?= Html::a('<i class="fa fa-plus"></i> Buat Artikel', 'javascript:addartikel()', ['class' => 'btn btn-sm btn-success']) ?>
         </div>
     </div>

     <?php
     $form = ActiveForm::begin([
         "options" => [
             "class" => "",
             // "id"    => "form-filter-juskeb",
             // 'onsubmit'=>'return true;',
         ]
     ]);
     ?>
    <div class="card">
       <!-- /.card-header -->
       <div class="card-body">
         <table id="tbl_artikel" class="table table-bordered table-striped">
                 <thead>
                 <tr>
                     <th style="width:5px;">ID.</th>
                     <th>Judul</th>
                     <th>Mini Deskripsi</th>
                     <!-- <th>Deskripsi</th> -->
                     <th>Tgl.Event</th>
                     <th>Valid Until</th>
                     <th>Action</th>
                 </tr>
                 </thead>
             </table>

         </div>
    </div>
     <?php ActiveForm::end(); ?>
 </div>

 <div id="modalArtikel" class="modal fade bs-example-modal" role="dialog" aria-hidden="true">
     <div class="modal-dialog modal-lg">
         <div class="modal-content">
             <div class="modal-header">
                 <h4 class="modal-title" id="modalTitleArtikel"></h4>
                 <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
             </div>
             <div class="modal-body table-responsive" id="modalBodyArtikel">
                 Loading ...
             </div>
             <!-- <div class="modal-footer">
               <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
               <button type="button" class="btn btn-primary" id="modalBtnSave">Save changes</button>
             </div> -->
         </div>
     </div>
 </div>

 <div id="modalDetArtikel" class="modal fade bs-example-modal" role="dialog" aria-hidden="true">
     <div class="modal-dialog modal-lg">
         <div class="modal-content">
             <div class="modal-header">
                 <h4 class="modal-title" id="modalTitleDetArtikel"></h4>
                 <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
             </div>
             <div class="modal-body table-responsive" id="modalBodyDetArtikel">
                 Loading ...
             </div>
             <!-- <div class="modal-footer">
               <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
               <button type="button" class="btn btn-primary" id="modalBtnSave">Save changes</button>
             </div> -->
         </div>
     </div>
 </div>


 <script type="text/javascript">
     // var t = null;
     $(document).ready(function () {
         // t.destroy();

        $('#tbl_artikel').DataTable({
        //      "processing": true,
        // "serverSide": true,
        scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
        columnDefs: [
            {
                // width: '20%', targets: 0,
                "targets": [ 0 ],
                "visible": false,
            },
            // { width: '35%', targets: 1 },
            // { width: '15%', targets: 2 },
            // { width: '15%', targets: 3 },
            // { width: '15%', targets: 4 }
        ],
        fixedColumns: true,
             "ajax": '<?php echo \Yii::$app->getUrlManager()->createUrl("adm/getdataartikel");?>',
             "columns": [
                 {"data": "id"},
                 {"data": "judul"},
                 {"data": "mini_desc"},
                 // {"data": "desc"},
                 {"data": "tgl_event"},
                 {"data": "valid_until"},
                 {
                     "orderable": false,
                     "data": 'fungsi',
                     "defaultContent": ''
                 },

             ],
             "order": [[0, 'desc']]
         });
     });

     function addartikel() {
         var url = "<?php echo \Yii::$app->getUrlManager()->createUrl(['adm/create']);?>";
         var title = "Buat Artikel";
         showModal(url, title);
     }

     function adddetartikel(id) {
         var url = "<?php echo \Yii::$app->getUrlManager()->createUrl(['adm/createdet']);?>?id=" + id;
         var title = "Buat Detail Artikel";
         showModalDetArtikel(url, title);
     }

     function updateartikel(id) {
         var url = "<?php echo \Yii::$app->getUrlManager()->createUrl(['adm/update']);?>?id=" + id;
         var title = "Update Artikel";
         showModal(url, title);
     }

     function updatedetartikel(id) {
         var url = "<?php echo \Yii::$app->getUrlManager()->createUrl(['adm/updatedet']);?>?id=" + id;
         var title = "Update Detail Artikel";
         showModalDetArtikel(url, title);
     }

     function detailartikel(id) {
         var url = "<?php echo \Yii::$app->getUrlManager()->createUrl(['adm/detartikel']);?>?id=" + id;
         var title = "Detail Artikel";
         showModal(url, title);
     }

     function showModal(url, title) {
         $("#modalTitleArtikel").empty();
         $("#modalTitleArtikel").html(title);

         $("#modalBodyArtikel").empty();
         $("#modalBodyArtikel").html("Loading ...");
         $("#modalBodyArtikel").load(url);

         $('#modalArtikel').modal({backdrop: 'static', keyboard: false});
         $("#modalArtikel").modal("show");
         return false;
     }

     function showModalDetArtikel(url, title) {
         $("#modalTitleDetArtikel").empty();
         $("#modalTitleDetArtikel").html(title);

         $("#modalBodyDetArtikel").empty();
         $("#modalBodyDetArtikel").html("Loading ...");
         $("#modalBodyDetArtikel").load(url);

         $('#modalDetArtikel').modal({backdrop: 'static', keyboard: false});
         $("#modalDetArtikel").modal("show");
         return false;
     }

     function deleteartikel(id) {
         {
             swal({
                 title: "Konfirmasi",
                 text: "Hapus Artikel ini?",
                 icon: "warning",
                 buttons: true,
                 dangerMode: true,
             }).then((ya) => {
                 if (ya) {
                     $.ajax({
                         type: "GET",
                         // data: {id:id},
                         dataType: "json",
                         contentType: false,
                         processData: false,
                         url: "<?=\Yii::$app->getUrlManager()->createUrl(['adm/delete'])?>?id=" + id,
                         beforeSend: function () {
                             swal({
                                 title: 'Harap Tunggu',
                                 text: "Sedang Menghapus Menu",
                                 icon: 'info',
                                 buttons: {
                                     cancel: false,
                                     confirm: false,
                                 },
                                 closeOnClickOutside: false,
                                 onOpen: function () {
                                     swal.showLoading()
                                 },
                                 closeOnEsc: false,
                             });
                         },
                         complete: function () {
                             swal.close()
                         },
                         success: function (result) {

                             swal(result.header, result.message, result.status);

                             if (result.status == "success") {
                                 window.location = "<?=\Yii::$app->getUrlManager()->createUrl(['adm/artikel'])?>";
                             }
                         },
                         error: function (xhr, ajaxOptions, thrownError) {
                             swal("Error!", "Terdapat Kesalahan saat menghapus Menu!", "error");
                         }
                     });
                 } else {

                 }
             });
         }

     }
 </script>
