<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>
<style type="text/css">
    .geserkanan {
        margin-right: 5px;
    }
</style>
<div class="box box-primary">
    <div class="box-body">
        <div class="artikel-form">

            <?php $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-4',
                        'wrapper' => 'col-sm-7',
                        // 'label' => 'col-md-4 col-sm-4 col-xs-12',
                        // 'wrapper' => 'col-md-6 col-sm-6 col-xs-12',
                    ],
                ],
                "options" => [
                    "id" => "form-detartikel",
                    "class" => "",
                    "data-parsley-validate" => "",
                    'onsubmit'=>'return false;',
                    'enctype'=>'multipart/form-data',
                    'novalidate' => 'novalidate'
                ]
            ]); ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php /* <?= $form->field($model, 'id_artikel')->dropDownList(Yii::$app->Logic->arrArtikel(), ['class' => 'form-control select validate[required]']); ?> */ ?>
                                <label class="control-label">Judul Artikel</label>
                                <input type="text" class="form-control" value="<?=$model2['judul']; ?>" name="ArtikelDetail['judul']" id="artikeldetail-judul" readonly='true'>
                                <input type="hidden" class="form-control" value="<?=$model2['id']; ?>" name="ArtikelDetail['id_artikel']" id="artikeldetail-id_artikel" readonly='true'>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'sesi')->textInput(['maxlength' => true, 'class' => 'form-control']); ?>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'ket')->textarea(['maxlength' => true, 'rows' => '5', 'class' => 'form-control']) ?>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <!-- <div class="row">
                                <div class="col-md-6"> -->
                                    <div class="form-group">
                                        <label class="control-label">Jumlah Tiket</label>
                                        <input type="text" class="form-control" type="number" value="<?= isset($model['jml_tiket']) ? number_format($model['jml_tiket'],0,',','.') : ""; ?>" placeholder="0" name="ArtikelDetail[jml_tiket]" id="artikeldetail-jml_tiket" onkeypress="javascript:return isNumber(event)">
                                    </div>
                                <!-- </div> -->
                                <!-- <div class="col-md-6"> -->
                                    <div class="form-group">
                                        <label class="control-label">Jumlah Terjual</label>
                                        <input type="text" class="form-control" type="number" value="<?= isset($model['jml_terjual']) ? number_format($model['jml_terjual'],0,',','.') : ""; ?>" placeholder="0" name="ArtikelDetail[jml_terjual]" id="artikeldetail-jml_terjual" onkeypress="javascript:return isNumber(event)">
                                    </div>
                                <!-- </div> -->
                                    <div class="form-group">
                                        <?= $form->field($model, 'harga')->textInput(['maxlength' => true, 'class' => 'form-control', 'id' => 'price']); ?>
                                    </div>
                                    <div class="form-group bootstrap-timepicker">
                                        <?= $form->field($model, 'jam_tayang')->textInput(['maxlength' => true, 'placeholder' => 'HH:MM', 'class' => 'form-control', 'id' => 'endTime']); ?>
                                    </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <?php /* <div class="row">
                <div class="col-md-12">
                    <div class="form-group required">
                                <label class="control-label" for="first-name">File Items</label>
                                <!-- <div> -->
                                    <div class="file-loading">
                                        <input type="file" class="form-control" name="file_items" id="file_items" accept="image/*" required/>
                                    </div>
                                <!-- </div> -->
                    </div>
                </div>
            </div> */ ?>

            <div class="row">
                <div class="col-md-12">
                    <?= Html::a($model->isNewRecord ? '<i class="fa fa-floppy-o" aria-hidden="true"></i> Create' : '<i class="fa fa-pencil" aria-hidden="true"></i> Update', $model->isNewRecord ? 'javascript:savedetartikel('.$id.')' : 'javascript:saveupddetartikel("' . $id . '")', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-success pull-right']) ?>
                    <!-- <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button> -->
                    <?= Html::button('<i class="fa fa-times" aria-hidden="true"></i> Close', ['class' => 'btn btn-danger pull-right geserkanan', 'data-dismiss' => 'modal']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".select").select2();
        $('#price').number( true );
        $('.form-tanggal').datepicker({
            format: 'yyyy-mm-dd',
            // startDate: '<?=date('d-m-Y')?>',
            autoclose: true
        });

        // $('.time').timepicker({
        //    defaultTime: '00:00',
        //    maxHours:'24',
        //    minuteStep :1,
        //    showHours : false,
        //    showMeridian:false,
        //    showInputs:true,
        //    explicitMode:true
        // });

        $('input[id$="endTime"]').inputmask({
                // "mask":":",
                "regex":"^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$"
            },
            {
                mask: "HH:MM",
                placeholder: "HH:MM",
                insertMode: false,
                showMaskOnHover: false,
                // timeseparator: ":",
                hourFormat: 24
            }
        );

        // rupiah.addEventListener('keyup', function(e){
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        // rupiah.value = formatRupiah(this.value);
        // });
    });
</script>
