<table id="tbl_detartikel" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th style="width:5px;">ID.</th>
            <th>Deskripsi</th>
            <th>Jumlah Tiket</th>
            <th>Jumlah Terjual</th>
            <th>Harga</th>
            <th>Sesi</th>
            <th>Jam Tayang</th>
            <th>Action</th>
        </tr>
    </thead>
</table>

<script>
// var t = null;
$(document).ready(function () {
    var idartikel = <?=$_GET['idartikel']?>;
    // alert(idartikel);
    // t.destroy();

   $('#tbl_detartikel').DataTable({
       scrollY:        "170px",
       scrollX:        true,
       scrollCollapse: true,
       columnDefs: [
           {
               // width: '35%', targets: 0,
               "targets": [ 0 ],
               "visible": false,
           },
           // { width: '15%', targets: 1 },
           // { width: '15%', targets: 2 },
           // { width: '15%', targets: 3 },
           // { width: '20%', targets: 4 }
       ],
       fixedColumns: true,
        "ajax": '<?php echo \Yii::$app->getUrlManager()->createUrl("adm/getdatadetartikel");?>?idartikel='+idartikel,
        "columns": [
            {"data": "id"},
            {"data": "ket"},
            {"data": "jml_tiket"},
            {"data": "jml_terjual"},
            {"data": "harga"},
            {"data": "sesi"},
            {"data": "jam_tayang"},
            {
                "orderable": false,
                "data": 'fungsi',
                "defaultContent": ''
            },

        ],
        "order": [[0, 'desc']]
    });
});
</script>
