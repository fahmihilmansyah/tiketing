<?php


/* @var $this yii\web\View */
/* @var $model app\models\Artikel */

?>
<div class="artikel-update">

    <?= $this->render('_form', [
        'model' => $model,
        'id' => $id
    ]) ?>

</div>
