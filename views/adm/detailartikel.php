<?php
/**
 * Created by PhpStorm.
 * Project : tiketing
 * User: fahmihilmansyah
 * Date: 2018-12-18
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 00:09
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
 ?>
 <?php

 use yii\bootstrap\ActiveForm;
 use yii\helpers\Html;
 use yii\helpers\Url;

 // use fedemotta\datatables\DataTables;

 /* @var $this yii\web\View */
 /* @var $searchModel app\models\PAsetKategoriSearch */
 /* @var $dataProvider yii\data\ActiveDataProvider */

 $this->title = 'Detail Artikel';
 $this->params['breadcrumbs'][] = $this->title;
 ?>
 <style type="text/css">
     .mright {
         margin-right: 5px;
     }
 </style>
<div class="detartikel-index">
    <?php /* <h1 class="title"><?= Html::encode($this->title) ?></h1> */ ?>
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div id="box-confirm">
                        <div id="information">
                            <p>
                                <span class="field">Judul</span>
                                <span class="value"><?=$modelArtikel['judul']?></span>
                            </p>
                            <p>
                                <span class="field">Deskripsi Singkat</span>
                                <span class="value"><?=$modelArtikel['mini_desc']?></span>
                            </p>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="box-confirm">
                        <div id="information">
                            <p>
                                <span class="field">Tanggal Event</span>
                                <span class="value"><?=$modelArtikel['tgl_event']?></span>
                            </p>
                            <p>
                                <span class="field">Valid Until</span>
                                <span class="value"><?=$modelArtikel['valid_until']?></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-12" align="right" style="margin-bottom:5px;">
                    <?php /* <?= Html::a('<i class="fa fa-plus"></i> Close', 'javascript:closemodal()', ['class' => 'btn btn-sm btn-success']) ?> */ ?>
                    <?= Html::a('<i class="fa fa-plus"></i> Tambah Detail', 'javascript:adddetartikel('.$modelArtikel['id'].')', ['class' => 'btn btn-sm btn-success']) ?>
                </div>
            </div>
            <div id = "tbltkt"></div>
        </div>
    </div>
    <div class="modal-footer">
      <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      <!-- <button type="button" class="btn btn-primary" id="modalBtnSave">Save changes</button> -->
    </div>
</div>

 <script type="text/javascript">
    var idartikel = <?=$modelArtikel['id'];?>;
    var url = "<?php echo \Yii::$app->getUrlManager()->createUrl("adm/loadtbl");?>?idartikel="+idartikel;
    $('#tbltkt').load(url);

    function reloadTbl(id)
    {
     var idartikel = <?=$modelArtikel['id'];?>;
     var url = "<?php echo \Yii::$app->getUrlManager()->createUrl("adm/loadtbl");?>?idartikel="+idartikel;
     $('#tbltkt').load(url);
    }

    function savedetartikel(id)
    {
        {
            swal({
                title: "Konfirmasi",
                text: "Tambah Detail Artikel ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((ya) =>
            {
                if (ya)
                {
                    var _data = new FormData($("#form-detartikel")[0]);
                    $.ajax({
                        type: "POST",
                        data: _data,
                        dataType: "json",
                        contentType: false,
                        processData: false,
                        url: "<?=\Yii::$app->getUrlManager()->createUrl(['adm/createdet'])?>?id="+id,
                        beforeSend: function () {
                             swal({
                                 title: 'Harap Tunggu',
                                 text: "Memasukan Detail Artikel Baru",
                                 icon: 'info',
                                 buttons: {
                                     cancel: false,
                                     confirm: false,
                                 },
                                 closeOnClickOutside: false,
                                 onOpen: function () {
                                     swal.showLoading()
                                 },
                                 closeOnEsc: false,
                             });
                        },
                        complete: function () {
                             swal.close();

                        },
                        success: function (result) {
                            swal(result.header, result.message, result.status);
                            if (result.status == "success") {
                                $('#modalDetArtikel').modal('hide');
                                reloadTbl(idartikel);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError)
                        {
                             swal("Error!", "Terdapat Kesalahan saat memasukan Menu!", "error");
                        }
                    });
                } else {

                }
            });
        }
    }

    function saveupddetartikel(id) {
     {
         swal({
             title: "Konfirmasi",
             text: "Ubah Detail Artikel ini?",
             icon: "warning",
             buttons: true,
             dangerMode: true,
         }).then((ya) => {
             if (ya) {
                 var _data = new FormData($("#form-detartikel")[0]);
                 $.ajax({
                     type: "POST",
                     data: _data,
                     dataType: "json",
                     contentType: false,
                     processData: false,
                     url: "<?=\Yii::$app->getUrlManager()->createUrl(['adm/updatedet'])?>?id=" + id,
                     beforeSend: function () {
                         swal({
                             title: 'Harap Tunggu',
                             text: "Mengubah Detail Artikel Baru",
                             icon: 'info',
                             buttons: {
                                 cancel: false,
                                 confirm: false,
                             },
                             closeOnClickOutside: false,
                             onOpen: function () {
                                 swal.showLoading()
                             },
                             closeOnEsc: false,
                         });
                     },
                     complete: function () {
                         swal.close()
                     },
                     success: function (result) {

                         swal(result.header, result.message, result.status);

                         if (result.status == "success") {
                             $('#modalDetArtikel').modal('hide');
                             reloadTbl(idartikel);
                         }
                     },
                     error: function (xhr, ajaxOptions, thrownError) {
                         swal("Error!", "Terdapat Kesalahan saat memasukan Menu!", "error");
                     }
                 });
             } else {
                 // swal("Informasi", "Dokumen Tidak Dihapus", "info");
             }
         });
     }
    }

    function deletedetartikel(id) {
     {
         swal({
             title: "Konfirmasi",
             text: "Hapus Detail Artikel ini?",
             icon: "warning",
             buttons: true,
             dangerMode: true,
         }).then((ya) => {
             if (ya) {
                 $.ajax({
                     type: "GET",
                     // data: {id:id},
                     dataType: "json",
                     contentType: false,
                     processData: false,
                     url: "<?=\Yii::$app->getUrlManager()->createUrl(['adm/deletedet'])?>?id=" + id,
                     beforeSend: function () {
                         swal({
                             title: 'Harap Tunggu',
                             text: "Sedang Menghapus Detail Artikel",
                             icon: 'info',
                             buttons: {
                                 cancel: false,
                                 confirm: false,
                             },
                             closeOnClickOutside: false,
                             onOpen: function () {
                                 swal.showLoading()
                             },
                             closeOnEsc: false,
                         });
                     },
                     complete: function () {
                         swal.close()
                     },
                     success: function (result) {

                         swal(result.header, result.message, result.status);

                         if (result.status == "success") {
                             var url = "<?php echo \Yii::$app->getUrlManager()->createUrl("adm/getdatadetartikel");?>?idartikel="+id;
                             $('#modalDetArtikel').modal('hide');
                             reloadTbl(idartikel);
                         }
                     },
                     error: function (xhr, ajaxOptions, thrownError) {
                         swal("Error!", "Terdapat Kesalahan saat menghapus Detail Artikel!", "error");
                     }
                 });
             } else {

             }
         });
     }

    }
 </script>
