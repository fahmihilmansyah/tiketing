 <?php

 use yii\bootstrap\ActiveForm;
 use yii\helpers\Html;

 // use fedemotta\datatables\DataTables;

 /* @var $this yii\web\View */
 /* @var $searchModel app\models\PAsetKategoriSearch */
 /* @var $dataProvider yii\data\ActiveDataProvider */

 $this->title = 'Home';
 $this->params['breadcrumbs'][] = $this->title;
 ?>
 <div class="menuartikel-index">

     <h1 class="title"><?= Html::encode($this->title) ?></h1>
</div>
