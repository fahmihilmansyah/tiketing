<?php


/* @var $this yii\web\View */
/* @var $model app\models\Artikel */

?>
<div class="artikel-create">

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
