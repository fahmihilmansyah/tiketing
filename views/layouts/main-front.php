<?php
/**
 * Created by PhpStorm.
 * Project : tiketing
 * User: fahmihilmansyah
 * Date: 2018-12-11
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 16:41
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use app\assets\FrontAsset;
use yii\helpers\Url;

FrontAsset::register($this);
$this->title = "Sensyar'i";
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <link rel="shortcut icon" href="<?= Url::to(['/faveicon.png']) ?>" type="image/x-icon">
    <link rel="icon" href="<?= Url::to(['/faveicon.png']) ?>" type="image/x-icon">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="H9T14PlpVfFmq1Xhb0jows5IqYAaw6nCXn-HqYICsHA" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <style media="screen">
        .close {
            position: absolute;
            right: 10px;
            top: 0;
            z-index: 1050;
            font-size: 3rem;
            font-weight: normal;
            opacity: 1
        }

        .buttonLoginRegister {
            transition: none 0s ease 0s;
            text-align: inherit;
            line-height: 40px;
            border-width: 0;
            margin: 10px;
            padding: 0 38px;
            letter-spacing: 0;
            font-weight: 600;
            font-size: 15px;
            background: #3897f0
        }

        .buttonLoginRegisterBackgroundWhite {
            transition: none 0s ease 0s;
            text-align: inherit;
            line-height: 40px;
            border-width: 0;
            margin: 10px;
            padding: 0 38px;
            letter-spacing: 0;
            font-weight: 600;
            font-size: 15px
        }

        .buttonLoginRegisterBackgroundGreen {
            transition: none 0s ease 0s;
            text-align: inherit;
            line-height: 40px;
            border-width: 0;
            margin: 10px;
            padding: 0 38px;
            letter-spacing: 0;
            font-weight: 600;
            font-size: 15px;
            background: #006400;
            color: #fff
        }

        .buttonKlipTanyaUHA {
            transition: none 0s ease 0s;
            text-align: inherit;
            line-height: 40px;
            border-width: 0;
            margin: 10px;
            padding: 0 38px;
            letter-spacing: 0;
            font-weight: 600;
            font-size: 15px;
            background: #d63970
        }

        .buttonKirimPertanyaan {
            transition: none 0s ease 0s;
            text-align: inherit;
            line-height: 40px;
            border-width: 0;
            margin: 10px;
            padding: 0 38px;
            letter-spacing: 0;
            font-weight: 600;
            font-size: 15px;
            background: #d3d639
        }

        .new-login {
            transition: none 0s ease 0s;
            text-align: inherit;
            line-height: 18px;
            border-width: 0;
            margin: 0;
            padding: 0;
            letter-spacing: 0;
            font-weight: 700;
            font-size: 15px
        }

        .value-new-jos {
            display: block;
            font-size: 37px;
            line-height: 35px;
            padding: 22px 0 0 0;
            color: #fff
        }

        .overlay-gradient .slotholder:after,
        .overlay-gradient:after {
            opacity: .4 !important
        }
        .imgcorausel{
            width: 100%; position: absolute; z-index: -1;
        }
    </style>
    <script>
        if (location.protocol != 'https:') { location.href = 'https:' + window.location.href.substring(window.location.protocol.length); }
    </script>
</head>
<body>
<?php $this->beginBody() ?>

<!-- =============== START OF PRELOADER =============== -->
<!-- <div class="loading">
    <div class="loading-inner">
        <div class="loading-effect">
            <div class="object"></div>
        </div>
    </div>
</div> -->
<!-- =============== END OF PRELOADER =============== -->
<!-- =============== START OF RESPONSIVE - MAIN NAV =============== -->
<nav id="main-mobile-nav"></nav>
<!-- =============== END OF RESPONSIVE - MAIN NAV =============== -->
<!-- =============== START OF WRAPPER =============== -->
<div class="wrapper">
    <!-- =============== START OF HEADER NAVIGATION =============== -->
    <!-- Insert the class "sticky" in the header if you want a sticky header -->
    <header class="header header-fixed header-transparent text-white">
        <div class="container-fluid">
            <!-- ====== Start of Navbar ====== -->
            <nav class="navbar navbar-expand-lg">
                <a class="navbar-brand" href="/"></a>
                <button id="mobile-nav-toggler" class="hamburger hamburger--collapse" type="button">
               <span class="hamburger-box">
                  <span class="hamburger-inner"></span>
               </span>
                </button>
                <!-- ====== Start of #main-nav ====== -->
                <div class="navbar-collapse" id="main-nav">

                    <!-- ====== Start of Main Menu ====== -->
                    <ul class="navbar-nav mx-auto full-width" id="main-menu">
                        <!-- Menu Item -->

                        <!-- Dropdown Menu -->
                        <li class="nav-item">
                            <a class="nav-link" href="/">Home</a>
                        </li>
                        <!-- Menu Item -->
                        <li class="nav-item">
                            <a class="nav-link tujuanscroll" href="#belitiketnya" data-tujuan="belitiketnya"><i class="fa fa-ticket"></i> Beli Tiket</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/contact-us">Contact us</a>
                        </li>

                    </ul>
                    <!-- ====== End of Main Menu ====== -->
                    <!-- ====== Start of Extra Nav ====== -->
                    <ul class="navbar-nav extra-nav">
                        <!-- Menu Item -->
                        <li class="nav-item">
                            <a class="nav-link toggle-search" href="#">
                                <i class="fa fa-search"></i>
                            </a>
                        </li>
                        <!-- Menu Item -->
                        <li class="nav-item notification-wrapper">
                            <a class="nav-link notification" href="#">
                                <i class="fa fa-globe"></i>
                                <span class="notification-count">2</span>
                            </a>
                        </li>
                        <!-- Menu Item -->
                        
                    </ul>
                    <!-- ====== End of Extra Nav ====== -->
                </div>
                <!-- ====== End of #main-nav ====== -->
            </nav>
            <!-- ====== End of Navbar ====== -->
        </div>
    </header>
    <!-- =============== END OF HEADER NAVIGATION =============== -->
    <!-- =============== START OF SLIDER SECTION =============== -->
    <section id="slider" class="full-height">
        <div id="main-slider" class="carousel slide full-height" data-ride="carousel">
            <?php $getdataartik = (new \yii\db\Query())->from('artikel')->andWhere(['>=','valid_until',date("Y-m-d")])->limit(2)->all(); ?>
            <ol class="carousel-indicators">
                <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                <?php $i=1; foreach ($getdataartik as $rs): ?>
                <li data-target="#main-slider" data-slide-to="<?php echo $i?>"></li>
                <?php $i++; endforeach; ?>
            </ol>
            <div class="carousel-inner full-height">
                <div class="carousel-item full-height active">
                    <div class="full-height valign-wrapper bg-slider" style="background-image:url(<?php echo \yii\helpers\Url::to(['/banner/baner1.jpg'],true)?>)">
                        <div class="valign-box text-center">
                            <div class="slider-title wow animated fadeInLeft" data-animation="animated fadeInLeft">#TeaterMusikal<br> Kepo Scroll Kebawah</div>
                            <h6 class="lh-1 mb-0 wow animated fadeInRight" data-animated="animated fadeInRight" data-wow-delay="0.2s">Ngerasa Udah Bayar?</h6>
                            <a href="https://api.whatsapp.com/send?phone=6281263310679&text=Assalamualaikum%2C+saya+ingin+konfirmasi+pembayaran"
                                               class="btn btn-main btn-effect buttonKlipTanyaUHA wow animated fadeInUp" data-wow-delay="1s"
                                               data-animation="animated fadeInUp" style="font-size: 19px !important;"><span class="fa fa-whatsapp"></span> KONFIRM STRUK</a>
                            <form  action="<?php echo \yii\helpers\Url::to(['/info/index']) ?>">
                                <input name="order" placeholder="#No Order">
                                <button type="submit" class="btn btn-primary">Cek Order</button>
                            </form>
                        </div>
                    </div>
                </div>
                <?php foreach ($getdataartik as $r):?>
                <div class="carousel-item full-height">
                    <div class="full-height valign-wrapper bg-slider" style="background-image:url(<?php echo $r['gambar']?>);">
                        <div class="valign-box text-center">
                            <div class="slider-title wow animated fadeInLeft" data-animation="animated fadeInLeft"><?php echo $r['judul'] ?></div>
                            <a href="#" data-url="<?php echo str_replace('http://','https://',\yii\helpers\Url::to(['/site/artikelbyr','id'=>$r['id']],true))?>"
                               class="buytiketing btn btn-main btn-effect buttonKlipTanyaUHA wow animated fadeInUp" data-wow-delay="1s"
                               data-animation="animated fadeInUp" style="font-size: 19px !important;"><?php echo $r['is_tiket'] == '1'? '<span class="fa fa-shopping-cart"></span> &nbsp; Beli Tiket':'<span class="fa fa-user"></span> &nbsp; Registrasi'?></a>
                        </div>
                    </div>
                </div>
                <?php
                endforeach;
                ?>
            </div>
            <a class="carousel-control-prev" href="#main-slider" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#main-slider" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>
    <!-- =============== END OF COUNTER SECTION =============== -->
    <section class="counter bg-main-gradient ptb50 text-center">
        <div class="container-fluid">
            <div class="row">

                <!-- 1st Count up item -->
                <div class="col-4 countbox">
                    <span class="counter-item" data-from="1" data-to="7">1</span>
                    <h4>Judul Teater Musikal</h4>
                </div>

                <!-- 2nd Count up item -->
                <div class="col-4 countbox">
                    <span class="counter-item" data-from="1" data-to="1300">1</span>
                    <h4>Pengikut Setia</h4>
                </div>

                <!-- 3rd Count up item -->
                <div class="col-4 countbox">
                    <span class="counter-item" data-from="1" data-to="15000">1</span>
                    <h4>Penonton</h4>
                </div>

            </div>
        </div>
    </section>
    <!-- =============== END OF BLOG SECTION =============== -->
    <!-- =============== END OF SLIDER SECTION =============== -->
    <?= $content ?>
    <!-- =============== START OF TOP MOVIES SECTION =============== -->
    <!-- =============== END OF TOP MOVIES SECTION =============== -->
    <section class="latest-releases bg-light ptb100" style="padding-bottom: 0px !important;">
        <div class="container">

            <!-- Start of row -->
            <div class="row justify-content-center">
                <div class="col-md-7 text-center">
                    <h2 class="title">Klip Highlight</h2>
                </div>
            </div>
            <!-- End of row -->
        </div>
        <!-- End of Container -->
    </section>

    <!-- =============== END OF LATEST RELEASES SECTION =============== -->


    <!-- =============== START OF FEATURES SECTION =============== -->
    <section class="features">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-6 col-sm-12"
                     >
                    
                    <iframe width="853" height="480" src="https://www.youtube.com/embed/DHgzhvxTJsE" frameborder="0" allowfullscreen></iframe>
                </div>

                <div class="col-md-6 col-sm-12 bg-light">
                    <div class="features-wrapper">
                        <h3 class="title">Klip Inspirasi dari Sensyari yang nggak bisa kamu temuin kecuali disini</h3>
                        <p>Kira-kira gimana ya ngalamin masalah keseharian yang temen-temen hadepin, In Syaa Alloh Teater Musikal Sensyari akan release disini, mudah-mudahan bisa jadi inspirasi.</p>
                        <!-- <a class="btn btn-main btn-effect" href="#">View Features</a> -->
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- =============== END OF FEATURES SECTION =============== -->



    <section class="blog bg-light ptb100">
        <div class="container">

            <!-- Start of row -->
            <div class="row justify-content-center">
                <div class="col-md-7 text-center">
                    <h2 class="title">#teaterMusikal</h2>
                    <h6 class="subtitle">Terimakasih, lewat partisipasi temen-temen kita jadi bisa belajar menebar manfaat.
                        Semoga lebih banyak lagi projek-projek kebaikan yang bisa dilakuin bareng-bareng.</h6>
                </div>
            </div>
            <!-- End of row -->
        </div>
        <!-- =============== END OF WRAPPER =============== -->

</div>
<!-- End of Latest Releases Slider -->
<footer class="footer1 bg-dark">
    <!-- ===== START OF FOOTER WIDGET AREA ===== -->
    <div class="footer-widget-area ptb100">
        <div class="container">
            <div class="row">
                <!-- Start of Widget 1 -->
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="widget widget-about">
                        <!-- INSERT YOUR LOGO HERE -->
                        <!-- INSERT YOUR WHITE LOGO HERE -->
                        <p class="nomargin">Mari temen-temen, lewat partisipasi kalian, kita bisa belajar
                            menebar manfaat. Semoga lebih banyak lagi projek-projek kebaikan yang bisa dilakuin
                            bareng-bareng.</p>
                    </div>
                </div>
                <!-- End of Widget 1 -->

                <!-- Start of Widget 2 -->
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="widget widget-links">
                        <h4 class="widget-title">Useful links</h4>

                        <ul class="general-listing">
                            <li><a href="#" target="_blank">Sensyari</a></li>
                            <li><a href="#"
                                   target="_blank">Sensyari</a></li>


                        </ul>

                    </div>
                </div>
                <!-- End of Widget 2 -->

                <!-- Start of Widget 3 -->
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="widget widget-blog">


                        <ul class="blog-posts">


                        </ul>
                    </div>
                </div>
                <!-- End of Widget 3 -->

                <!-- Start of Widget 4 -->

                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="widget widget-social">
                        <h4 class="widget-title">follow us</h4>

                        <div class="row">
                            <!-- Start of Social Buttons -->
                            <ul class="social-btns" style="margin-top: 8px !important;">
                                <!-- Social Media -->
                                <li>
                                    <a href="http://www.twitter.com/sensyari"
                                       class="social-btn-roll instagram" target="_blank">
                                        <div class="social-btn-roll-icons">
                                            <i class="social-btn-roll-icon fa fa-twitter"></i>
                                        </div>
                                    </a>

                                </li>
                            </ul>

                            <!-- <h6 style="color: white !important;">#tanyaMusikal</h6> -->

                            <h6 style="color: white !important;">
                                <a href="http://www.twitter.com/sensyari" class="alert-link"><u
                                            style="font-size: 19px !important;">Sensyari</u></a></h6>
                        </div>

                        <div class="row">
                            <!-- Start of Social Buttons -->
                            <ul class="social-btns" style="margin-top: 8px !important;">
                                <!-- Social Media -->
                                <li>
                                    <a href="https://instagram.com/sensyari?utm_source=ig_profile_share&igshid=155hncjgtsj26aa"
                                       class="social-btn-roll instagram">
                                        <div class="social-btn-roll-icons">
                                            <i class="social-btn-roll-icon fa fa-instagram"></i>
                                        </div>
                                    </a>

                                </li>
                            </ul>

                            <h6 style="color: white !important;">
                                <a href="#" class="alert-link"><u
                                            style="font-size: 19px !important;">Sensyari</u></a></h6>

                        </div>

                        <div class="row">
                            <!-- Start of Social Buttons -->
                            <ul class="social-btns" style="margin-top: 8px !important;">
                                <!-- Social Media -->
                                <li>
                                    <a href="http://www.facebook.com/sensyari"
                                       class="social-btn-roll instagram">
                                        <div class="social-btn-roll-icons">
                                            <i class="social-btn-roll-icon fa fa-facebook"></i>
                                        </div>
                                    </a>

                                </li>
                            </ul>

                            <h6 style="color: white !important;">
                                <a href="#" class="alert-link"><u
                                            style="font-size: 19px !important;">Sensyari</u></a></h6>

                        </div>

                        <!-- End of Social Buttons -->

                    </div>
                </div>
                <!-- End of Widget 4 -->

            </div>
        </div>
    </div>
    <!-- ===== END OF FOOTER WIDGET AREA ===== -->

    <!-- ===== Start of Back to Top Button ===== -->
    <div id="backtotop">
        <a href="#"></a>
    </div>
    <!-- ===== End of Back to Top Button ===== -->

    <!-- ===== START OF FOOTER COPYRIGHT AREA ===== -->
    <div class="footer-copyright-area ptb30">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex">
                        <div class="links">
                            <ul class="list-inline">
                                <li class="list-inline-item"><a href="#">Privacy & Cookies</a></li>
                                <li class="list-inline-item"><a href="#">Terms & Conditions</a></li>
                                <li class="list-inline-item"><a href="#">Legal Disclaimer</a></li>
                                <li class="list-inline-item"><a href="#">Community</a></li>
                            </ul>
                        </div>

                        <div class="copyright ml-auto">All Rights Reserved by <a href="http://fb.me/fahmihilmansyah"
                                                                                 target="_blank">FhhDev</a>.
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ===== END OF FOOTER COPYRIGHT AREA ===== -->

</footer>
<!-- =============== END OF FOOTER =============== -->
<?php $this->endBody() ?>


<script>
    new WOW().init();

    var $myCarousel = $('#main-slider');
    // Initialize carousel
    $myCarousel.carousel({
        interval: 9000,
        pause: "hover",
    });

    function doAnimations(elems) {
      var animEndEv = 'webkitAnimationEnd animationend';

      elems.each(function () {
        var $this = $(this),
            $animationType = $this.data('animation');

        // Add animate.css classes to
        // the elements to be animated
        // Remove animate.css classes
        // once the animation event has ended
        $this.addClass($animationType).one(animEndEv, function () {
          $this.removeClass($animationType);
        });
      });
    }

    // Select the elements to be animated
    // in the first slide on page load
    var $firstAnimatingElems = $myCarousel.find('.item:first')
                               .find('[data-animation ^= "animated"]');

    // Apply the animation using our function
    doAnimations($firstAnimatingElems);

    // Pause the carousel
    $myCarousel.carousel('pause');

    // Attach our doAnimations() function to the
    // carousel's slide.bs.carousel event
    $myCarousel.on('slide.bs.carousel', function (e) {
      // Select the elements to be animated inside the active slide
      var $animatingElems = $(e.relatedTarget)
                            .find("[data-animation ^= 'animated']");
      doAnimations($animatingElems);
    });
</script>
<script>

    $('.tujuanscroll').on('click',function () {
        var target = this.hash;
        $('html,body').animate({
            scrollTop: $( target).offset().top
        }, 'slow');
    })
</script>
</body>
</html>
<?php $this->endPage() ?>
