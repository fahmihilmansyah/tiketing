<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Url;

?>
<!-- Navbar -->
<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>
        <!-- <li class="nav-item d-none d-sm-inline-block">
            <a href="index3.html" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Contact</a>
        </li> -->
    </ul>
</nav>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="<?php echo \yii\helpers\Url::to(['/AdminLTE-3.0.0-alpha.2/dist/img/AdminLTELogo.png'])?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Admin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <!-- <li class="nav-item">
                    <a href="<?//= Url::to(['/adm/home']);?>" class="nav-link">
                        <i class="nav-icon fa fa-th"></i>
                        <p>
                            Home

                        </p>
                    </a>
                </li> -->
                <li class="nav-item has-treeview">
                  <a href="#" class="nav-link">
                    <i class="nav-icon fa fa-pie-chart"></i>
                    <p>
                      Manajemen master
                      <i class="right fa fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="<?= Url::to(['adm/artikel']);?>" class="nav-link">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Artikel</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="<?= Url::to(['/users']);?>" class="nav-link">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Users</p>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="<?= Url::to(['/tiketing']);?>" class="nav-link">
                        <i class="nav-icon fa fa-ticket"></i>
                        <p>
                            Tiketing
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= Url::to(['adm/logout']);?>" class="nav-link">
                        <i class="nav-icon fa fa-circle-o text-info"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
