<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<style type="text/css">
.swal-overlay {
    z-index: 30000000;
}
</style>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <link rel="shortcut icon" href="<?= Url::to(['/faveicon.png']) ?>" type="image/x-icon">
    <link rel="icon" href="<?= Url::to(['/faveicon.png']) ?>" type="image/x-icon">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> -->
    <?php /* <link href="<?= Url::base().'/bootstrap-fileinput/fileinput.min.css';?>" media="all" rel="stylesheet" type="text/css" />

    <script src="<?= Url::base().'/bootstrap-fileinput/piexif.min.js';?>" type="text/javascript"></script>
    <!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview.
        This must be loaded before fileinput.min.js -->
    <script src="<?= Url::base().'/bootstrap-fileinput/sortable.min.js';?>" type="text/javascript"></script>
    <!-- purify.min.js is only needed if you wish to purify HTML content in your preview for
        HTML files. This must be loaded before fileinput.min.js -->
    <script src="<?= Url::base().'/bootstrap-fileinput/purify.min.js';?>" type="text/javascript"></script>

    <script src="<?= Url::base().'/bootstrap-fileinput/fileinput.min.js';?>"></script>
    <!-- following theme script is needed to use the Font Awesome 5.x theme (`fas`) -->
    <script src="<?= Url::base().'/bootstrap-fileinput/theme.min.js';?>"></script> */ ?>

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.1/js/locales/LANG.js"></script> -->
</head>
<body class="hold-transition sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">
    <?= $this->render('nav.php'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <?php /* <?= Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]) ?>
                        <?= Alert::widget() ?> */ ?>
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<?php /* <footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer> */ ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
