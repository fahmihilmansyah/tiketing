<?php
/**
 * Created by PhpStorm.
 * Project : tiketing
 * User: fahmihilmansyah
 * Date: 2018-12-18
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 00:09
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
 ?>
 <?php

 use yii\bootstrap\ActiveForm;
 use yii\helpers\Html;

 // use fedemotta\datatables\DataTables;

 /* @var $this yii\web\View */
 /* @var $searchModel app\models\PAsetKategoriSearch */
 /* @var $dataProvider yii\data\ActiveDataProvider */

 $this->title = 'Users';
 $this->params['breadcrumbs'][] = $this->title;
 ?>
 <style type="text/css">
     .mright {
         margin-right: 5px;
     }

     #filternya {
         margin-left: 20%;
         width: 70%;
         position: absolute;
         z-index: 10;
     }
    /* .modal-lg {
        max-width: 60%;
    } */
 </style>
 <div class="menuusers-index">

     <h1 class="title"><?= Html::encode($this->title) ?></h1>

     <!-- <p class="pull-right"> -->
     <div class="row">
         <div class="col-md-12" align="right" style="margin-bottom:5px;">
             <?= Html::a('<i class="fa fa-plus"></i> Buat Users', 'javascript:addusers()', ['class' => 'btn btn-success']) ?>
         </div>
     </div>

     <?php
     $form = ActiveForm::begin([
         "options" => [
             "class" => "",
             // "id"    => "form-filter-juskeb",
             // 'onsubmit'=>'return true;',
         ]
     ]);
     ?>
    <div class="card">
       <!-- /.card-header -->
       <div class="card-body">
         <table id="tbl_users" class="table table-bordered table-striped">
                 <thead>
                 <tr>
                     <!-- <th style="width:5px;">No.</th> -->
                     <th>Username</th>
                     <th>Email</th>
                     <!-- <th>Deskripsi</th> -->
                     <th>Role</th>
                     <th style="width:100px;">Action</th>
                 </tr>
                 </thead>
             </table>

         </div>
    </div>
     <?php ActiveForm::end(); ?>
 </div>

 <div id="modalUsers" class="modal fade bs-example-modal" role="dialog" aria-hidden="true">
     <div class="modal-dialog modal-lg">
         <div class="modal-content">
             <div class="modal-header">
                 <!-- <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button> -->
                 <h4 class="modal-title" id="modalTitleUsers"></h4>
             </div>
             <div class="modal-body" id="modalBodyUsers">
                 Loading ...
             </div>
             <!-- <div class="modal-footer">
               <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
               <button type="button" class="btn btn-primary" id="modalBtnSave">Save changes</button>
             </div> -->
         </div>
     </div>
 </div>

 <script type="text/javascript">
     // var t = null;
     $(document).ready(function () {
         // t.destroy();

         $('#tbl_users').DataTable({
        //      "processing": true,
        // "serverSide": true,
             "ajax": '<?php echo \Yii::$app->getUrlManager()->createUrl("users/getdatausers");?>',
             "columns": [
                 {"data": "username"},
                 {"data": "email"},
                 // {"data": "desc"},
                 {"data": "role"},
                 {
                     "orderable": false,
                     "data": 'fungsi',
                     "defaultContent": ''
                 },

             ],
             "order": [[1, 'asc']]
         });
     });

     function addusers() {
         var url = "<?php echo \Yii::$app->getUrlManager()->createUrl(['users/create']);?>";
         var title = "Buat Users";
         showModal(url, title);
     }

     function updateusers(id) {
         var url = "<?php echo \Yii::$app->getUrlManager()->createUrl(['users/update']);?>?id=" + id;
         var title = "Update Users";
         showModal(url, title);
     }

     function showModal(url, title) {
         $("#modalTitleUsers").empty();
         $("#modalTitleUsers").html(title);

         $("#modalBodyUsers").empty();
         $("#modalBodyUsers").html("Loading ...");
         $("#modalBodyUsers").load(url);

         $('#modalUsers').modal({backdrop: 'static', keyboard: false});
         $("#modalUsers").modal("show");
         return false;
     }

     function deleteusers(id) {
         {
             swal({
                 title: "Konfirmasi",
                 text: "Hapus Users ini?",
                 icon: "warning",
                 buttons: true,
                 dangerMode: true,
             }).then((ya) => {
                 if (ya) {
                     $.ajax({
                         type: "GET",
                         // data: {id:id},
                         dataType: "json",
                         contentType: false,
                         processData: false,
                         url: "<?=\Yii::$app->getUrlManager()->createUrl(['users/delete'])?>?id=" + id,
                         beforeSend: function () {
                             swal({
                                 title: 'Harap Tunggu',
                                 text: "Sedang Menghapus Users",
                                 icon: 'info',
                                 buttons: {
                                     cancel: false,
                                     confirm: false,
                                 },
                                 closeOnClickOutside: false,
                                 onOpen: function () {
                                     swal.showLoading()
                                 },
                                 closeOnEsc: false,
                             });
                         },
                         complete: function () {
                             swal.close()
                         },
                         success: function (result) {

                             swal(result.header, result.message, result.status);

                             if (result.status == "success") {
                                 window.location = "<?=\Yii::$app->getUrlManager()->createUrl(['users'])?>";
                             }
                         },
                         error: function (xhr, ajaxOptions, thrownError) {
                             swal("Error!", "Terdapat Kesalahan saat menghapus Menu!", "error");
                         }
                     });
                 } else {

                 }
             });
         }

     }
 </script>
