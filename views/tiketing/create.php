<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TiketBooking */

$this->title = 'Create Tiket Booking';
$this->params['breadcrumbs'][] = ['label' => 'Tiket Bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tiket-booking-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
