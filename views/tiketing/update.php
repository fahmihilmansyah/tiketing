<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TiketBooking */

$this->title = 'Update Tiket Booking: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Tiket Bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tiket-booking-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
