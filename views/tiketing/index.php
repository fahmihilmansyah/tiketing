<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tiket Bookings';
?>

<script>
    if (location.protocol != 'https:') { location.href = 'https:' + window.location.href.substring(window.location.protocol.length); }
</script>
<div class="tiket-booking-index">
    <div class="card">
        <div class="card-header">
            <h3><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body table-responsive">

            <?php $data = $dataProvider->query->all();
            ?>
            <table class="table table-striped" id="tiketsdt">
                <thead>
                <tr>
                    <th>Member</th>
                    <th>Telp</th>
                    <th>Email</th>
                    <th>Domisili</th>
                    <th>No. Order</th>
                    <th>Produk</th>
                    <th>Sesi</th>
                    <th>Seat</th>
                    <th>Jumlah Tiket</th>
                    <th>Harga Tiket</th>
                    <th>Total Harga</th>
                    <th>Tgl Order</th>
                    <th>Tgl Proses</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data as $r): ?>
                    <tr>
                        <td>
                            <?php echo $r['fullname'] ?>
                        </td>
                        <td><?php echo $r['phone'] ?></td>
                        <td><?php echo $r['email'] ?></td>
                        <td><?php echo $r['domisili'] ?></td>
                        <td><?php echo $r['no_order'] ?></td>
                        <td>
                            <?php echo $r['judul'] ?><br>
                        </td>
                        <td><?php echo $r['sesi'] ?> <?php echo $r['jam_tayang'] ?></td>
                        <td><?php echo $r['ket'] ?></td>
                        <td><?php echo $r['jml_tiket'] ?></td>
                        <td><?php echo $r['harga'] ?></td>
                        <td><?php echo $r['total_harga'] ?></td>
                        <td><?php echo $r['created_by'] ?></td>
                        <td><?php echo $r['updated_by'] ?></td>
                        <td><label class="btn <?php echo $r['status'] == 'waiting' ? "btn-outline-success" :"btn-outline-danger" ?>"><?php echo $r['status'] ?></label></td>
                        <td>
                            <?php if($r['status'] == 'waiting'): ?>
                                <button data-url="<?php echo str_replace('http://', 'https://', \yii\helpers\Url::to(['/tiketing/proses'], true)) ?>"
                                        data-id="<?php echo $r['id'] ?>" class="btnappr btn btn-sm btn-outline-primary"
                                        data-valuecsrf="<?=Yii::$app->request->csrfToken?>"
                                        title="Approve tiket"><span class="fa fa-check"></span></button>
                                <button data-url="<?php echo str_replace('http://', 'https://', \yii\helpers\Url::to(['/tiketing/proses'], true)) ?>"
                                        data-id="<?php echo $r['id'] ?>" class="btncancel btn btn-sm btn-outline-primary"
                                        data-valuecsrf="<?=Yii::$app->request->csrfToken?>"
                                        title="Cancel Tiket" class="btncancel btn btn-sm btn-outline-danger"><span class="fa fa-trash"></span>
                                </button>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>


</div>
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script>
    $(document).ready( function () {
        $("#tiketsdt").DataTable({
            dom: 'Bfrtip',
            buttons: [
                // 'copyHtml5',
                'excelHtml5',
                // 'csvHtml5',
                // 'pdfHtml5'
            ]
        });
    });
    jQuery(document).on('click','.btnappr', function () {
        var iddt = $(this).attr('data-id');
        var urls = $(this).attr('data-url');
        var valuecsrf = $(this).attr('data-valuecsrf');
        swal({
            title: "Are you sure?",
            text: "Anda akan Approve orderan ini!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: urls,
                        method: "POST",
                        data: {idtiket: iddt, _csrf:valuecsrf, reqproses:'approve'},
                        dataType: "json",
                        beforeSend:function(){
                            swal("Sedang Diproses");
                        },
                        success:function (msg) {
                            if(msg.rc == 200){
                                swal(msg.msg);
                            }else{ swal(msg.msg);}
                            document.location.reload(true);
                        }
                    });
                }
            });
    });
    jQuery(document).on('click','.btncancel', function () {
        var iddt = $(this).attr('data-id');
        var urls = $(this).attr('data-url');
        var valuecsrf = $(this).attr('data-valuecsrf');
        swal({
            title: "Are you sure?",
            text: "Anda akan membatalkan orderan!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: urls,
                        method: "POST",
                        data: {idtiket: iddt, _csrf:valuecsrf, reqproses:'cancel'},
                        dataType: "json",
                        beforeSend:function(){
                            swal("Sedang Diproses");
                        },
                        success:function (msg) {
                            if(msg.rc == 200){
                                swal(msg.msg);
                            }else{ swal(msg.msg);}
                            document.location.reload(true);
                        }
                    });
                }
            });
    })
</script>
