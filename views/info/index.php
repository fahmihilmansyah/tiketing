<?php
/**
 * Created by PhpStorm.
 * Project : tiketing
 * User: fahmihilmansyah
 * Date: 2018-12-22
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 02:28
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="google-site-verification" content="H9T14PlpVfFmq1Xhb0jows5IqYAaw6nCXn-HqYICsHA"/>
    <title>Bukti Pembayaran</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        body, html {
            padding-top: 50px;
            height: 100%;
        }
    </style>
</head>
<body>
<div class="container h-100">
    <div class="card">
        <div class="card-body">
            <div class="row h-100 justify-content-center align-items-center">

                <div class="col-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="text-center">Detail Order : #<?php echo $tiket['no_order'] ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <table class="table">
                                            <!--                                                <tr>-->
                                            <!--                                                    <td colspan="3" class="text-center">Terimakasih telah melakukan pembayaran</td>-->
                                            <!--                                                </tr>-->
                                            <tr>
                                                <?php if ($tiket['status'] == 'waiting'): ?>
                                                <td colspan="3" class="text-center">Silakan Bayar ke Bank:</td>
                                                <?php endif ?>
                                                <?php if ($tiket['status'] == 'konfirmasi'): ?>
                                                <td colspan="3" class="text-center">Terima kasih Sudah Konfirmasi</td>
                                                <?php endif ?>
                                                <?php if ($tiket['status'] == 'cancel'): ?>
                                                <td colspan="3" class="text-center">Mohon Maaf Order anda dibatalkan</td>
                                                <?php endif ?>
                                            </tr>
                                            <?php if ($tiket['status'] == 'waiting'): ?>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <img width="300"
                                                         src="https://vectorlogo4u.com/wp-content/uploads/2018/09/bank-bjb-vector-logo.png">
                                                    <br>
                                                    <span>No. Rek: 000-1122-221188</span>
                                                    <br>
                                                    <span>Atas nama : Yayasan Sensyari Indonesia</span>
                                                </td>
                                            </tr>
                                            <?php endif;?>
                                            <tr>
                                                <td>Diskon</td>
                                                <td>:</td>
                                                <td> <?php echo $tiket['diskon'] > 0 ? number_format( $tiket['diskon'],1,'.','').'%':'-' ?></td>
                                            </tr>
                                            <tr>
                                                <td>Jumlah Tagihan</td>
                                                <td>:</td>
                                                <td>Rp <?php echo $tiket['diskon'] > 0 ? '<strike>'.number_format( $tiket['total_tagihan'],0,'','.').'</strike> | Rp '. number_format( $tiket['total_harga'],0,'','.') : number_format( $tiket['total_tagihan'],0,'','.') ?></td>
                                            </tr>
                                            <tr>
                                                <td>Status</td>
                                                <td>:</td>
                                                <td>
                                                    <?php if ($tiket['status'] == 'waiting'): ?>
                                                        Waiting until [<?php echo date("m-d-Y") ?> 00:00:00]
                                                    <?php else:
                                                        echo $tiket['status'];
                                                        ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>