/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100309
 Source Host           : localhost:3306
 Source Schema         : tiketing

 Target Server Type    : MySQL
 Target Server Version : 100309
 File Encoding         : 65001

 Date: 17/12/2018 10:26:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for artikel
-- ----------------------------
DROP TABLE IF EXISTS `artikel`;
CREATE TABLE `artikel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) DEFAULT NULL,
  `desc` longtext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `gambar` varchar(500) DEFAULT NULL,
  `mini_desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of artikel
-- ----------------------------
BEGIN;
INSERT INTO `artikel` VALUES (1, 'Mencari Kesuksesan', '<p>SEJATINYA setiap manusia menyadari bahwa hidupnya di dunia akan bertemu titik akhir berupa kematian. Saat kematian itu tiba, sirnalah segala kenikmatan hidup. Tinggallah manusia sebatang kara, terbujur kaku di dalam kubur.</p>\n<p>Namun, rasio manusia tidak kehilangan cahaya kala berbicara kematian. Sebab, ternyata kematian adalah satu jalan untuk manusia dapat terangkat semua hijab pandangan mata hatinya terhadap hakikat dari kebenaran dan kehidupan itu sendiri.</p>\n<p>Oleh karena itu, Islam memberikan penjelasan bahwa kehidupan di dunia ini laksana pertanian menuju akhirat. Siapa yang menanam kebaikan ia akan memperoleh kebaikan dan sebaliknya.<br />Imam Ghazali dalam Ihya&rsquo; Ulumuddin berkata, &ldquo;Tidaklah mungkin untuk menghasilkan bibit (tanaman) ini kecuali di dunia, tidak ditanam, kecuali pada kalbu dan tidak dipanen kecuali di akhirat.&rdquo;</p>\n<p>Kemudian Al-Ghazali mengutip hadits Nabi, &ldquo;Kebahagiaan yang paling utama adalah panjang umur di dalam taat kepada Allah Subhanahu wa Ta&rsquo;ala.&rdquo;</p>\n<p>Dalam kata yang lain, jika ditanya, siapa manusia yang beruntung dan bahagia, adalah yang menjadikan dunia sebagai ladang beramal, &ldquo;bercocok tanam&rdquo; untuk kebaikan akhiratnya. Dalam hal ini, ayat Al-Qur&rsquo;an sangat eskplisit menjelaskan.</p>\n<p>فَأَمَّا مَن ثَقُلَتْ مَوَازِينُهُ<br />فَهُوَ فِي عِيشَةٍ رَّاضِيَةٍ<br />وَأَمَّا مَنْ خَفَّتْ مَوَازِينُهُ<br />فَأُمُّهُ هَاوِيَةٌ</p>\n<p><em>&ldquo;Dan adapun orang-orang yang berat timbangan (kebaikan)nya, maka dia berada dalam kehidupan yang memuaskan. Dan adapun orang-orang yang ringan timbangan (kebaikan)nya, maka tempat kembalinya adalah Neraka Hawiyah.&rdquo;</em>&nbsp;(QS. Al Qari&rsquo;ah [101]: 6-9).</p>\n<blockquote>\n<p>Baca:&nbsp;&nbsp;<a href=\"https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=0ahUKEwiLlqSPibvYAhWKuo8KHTDxCJcQFggrMAA&amp;url=http%3A%2F%2Fwww.hidayatullah.com%2Fkajian%2Ftazkiyatun-nafs%2Fread%2F2017%2F12%2F20%2F131050%2Fpergunakan-usia-panjangmu-dengan-amal-shalih-1.html&amp;usg=AOvVaw3JG4yohIM7kp7diD4E7V9z\" data-cthref=\"/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=0ahUKEwiLlqSPibvYAhWKuo8KHTDxCJcQFggrMAA&amp;url=http%3A%2F%2Fwww.hidayatullah.com%2Fkajian%2Ftazkiyatun-nafs%2Fread%2F2017%2F12%2F20%2F131050%2Fpergunakan-usia-panjangmu-dengan-amal-shalih-1.html&amp;usg=AOvVaw3JG4yohIM7kp7diD4E7V9z\">Pergunakan Usia Panjangmu dengan Amal Shalih (1)</a></p>\n</blockquote>\n<p>Dengan demikian sebenarnya cukup sederhana memahami tentang bagaimana semestinya kaum Muslimin memandang kehidupan dunia, yakni bagaimana amal kebaikannya lebih unggul daripada amal keburukannya.</p>\n<p>Terlebih secara gamblang Allah juga telah menyebutkan bahwa diciptakannya kehidupan dan kematian ini hanyalah untuk menguji kehidupan umat manusia, dan mengetahui siapa yang terbaik amalnya.</p>\n<p>الَّذِي خَلَقَ الْمَوْتَ وَالْحَيَاةَ لِيَبْلُوَكُمْ أَيُّكُمْ أَحْسَنُ عَمَلاً وَهُوَ الْعَزِيزُ الْغَفُورُ</p>\n<p><em>&ldquo;Yang menjadikan mati dan hidup, supaya Dia menguji kamu, siapa di antara kamu yang lebih baik amalnya. Dan Dia Maha Perkasa lagi Maha Pengampun.</em>&rdquo; (QS. Al-Mulk [67]: 2).</p>\n<p>وَهُوَ الَّذِي خَلَق السَّمَاوَاتِ وَالأَرْضَ فِي سِتَّةِ أَيَّامٍ وَكَانَ عَرْشُهُ عَلَى الْمَاء لِيَبْلُوَكُمْ أَيُّكُمْ أَحْسَنُ عَمَلاً</p>\n<p><em>&ldquo;Dan Dia-lah yang menciptakan langit dan bumi dalam enam masa, dan adalah singgasana-Nya (sebelum itu) di atas air, agar Dia menguji siapakah di antara kamu yang lebih baik amalnya</em>.&rdquo; (QS: Hud [11]: 7).</p>\n<p>Memahami hal tersebut, hati kita akan semakin terang kala melihat sosok para sahabat menjadikan dunia sebagai bekal untuk akhirat.</p>\n<p>Sebut saja pebisnis ulung masa Nabi, Abdurrahman bin Auf, seluruh hasil dari perniagaannya ia salurkan untuk menyantuni para veteran perang Badar, para janda Rasulullah, dan memberi makan anak yatim dan fakir miskin di Madinah.</p>\n<p>Tidak saja mereka yang diberi Allah rezeki berupa harta, yang memiliki potensi pada sisi lainnya dan dengan kekuatan apapun yang mereka miliki, mereka tidak pernah lemah, loyo, apalagi letoy dalam mengisi kehidupan dunia dengan kebaikan demi kebaikan.</p>\n<p>Abdullah bin Amr misalnya, sejak awal menjadi Muslim, ia telah memusatkan perhatiannya terhadap Al-Qur&rsquo;an. Setiap turun ayat, ia langsung menghafalkan dan berusaha keras untuk memahaminya, hingga setelah semuanya selesai dan sempurna, ia pun telah hafal seluruhnya.<br />Kemudian dari sisi kecerdasan intelektual, lihatlah Muadz bin Jabal. Kecerdasan otak dan keberaniannya mengemukakan pendapat dikenal oleh seluruh penduduk Madinah. Sampai-sampai dikatakan Mu&rsquo;adz hampir sama dengan Umar bin Khathab.</p>\n<p>Namun kecerdasannya bukan untuk merengkuh keuntungan pribadi dan menghimpun kekayaan dunia. Tetapi membela agama Allah. Hal ini terbukti kala Rasulullah Shallallahu alayhi wasallam hendak mengirimnya ke Yaman. Beliau bertanya, &ldquo;Apa yang menjadi pedomanmu dalam mengadili sesuatu, hai Mu&rsquo;adz?&rdquo;</p>\n<p>&ldquo;Kitabullah,&rdquo; jawab Mu&rsquo;adz.</p>', '2018-12-14 14:20:17', '2018-12-14 14:20:22', NULL, 'https://www.hidayatullah.com/files/bfi_thumb/doa-34s9upah0fvd6134msfcao.jpg', 'SEJATINYA setiap manusia menyadari bahwa hidupnya di dunia akan bertemu titik akhir berupa kematian. Saat kematian itu tiba, sirnalah segala kenikmatan hidup. Tinggallah manusia sebatang kara, terbujur kaku di dalam kubur.');
COMMIT;

-- ----------------------------
-- Table structure for artikel_detail
-- ----------------------------
DROP TABLE IF EXISTS `artikel_detail`;
CREATE TABLE `artikel_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_artikel` int(11) DEFAULT NULL,
  `ket` varchar(255) DEFAULT NULL COMMENT 'untuk sesi',
  `jml_tiket` decimal(50,2) DEFAULT NULL,
  `jml_terjual` decimal(50,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of artikel_detail
-- ----------------------------
BEGIN;
INSERT INTO `artikel_detail` VALUES (1, 1, 'sesi_1', 200.00, 0.00, '2018-12-14 14:22:12', '2018-12-14 14:22:16', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id` varchar(50) NOT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for tiket_booking
-- ----------------------------
DROP TABLE IF EXISTS `tiket_booking`;
CREATE TABLE `tiket_booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_member` int(11) DEFAULT NULL,
  `id_artikel_detail` int(11) DEFAULT NULL,
  `jml_tiket` decimal(50,0) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL COMMENT 'konfirmasi,menunggu,terbayar,batal',
  `created_by` datetime DEFAULT NULL,
  `updated_by` datetime DEFAULT NULL,
  `jenis` varchar(255) DEFAULT NULL COMMENT 'jenis tiket offline online',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `authKey` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `accessToken` varchar(32) NOT NULL,
  `role` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'admin', '', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'saepuloh52@yahoo.com', '', 1, NULL, NULL);
INSERT INTO `users` VALUES (6, 'admin2', '7-16Ah9nQ-KxCStehpeAPVc2xdLVbpoA', '2053d551f2308e88a59faaee97b0c5d07d52a597', 'akh.saepuloh@gmail.com', '', 2, NULL, NULL);
INSERT INTO `users` VALUES (11, '850065', 'Gqmavv5EMKtU99fIfGW-kg0OlZos2B87', 'bbfa8f5e156aa4090048de1c40fcdb00fbd3d2a7', '850065@telkom.co.id', '', 2, NULL, '2017-08-09 10:55:59');
INSERT INTO `users` VALUES (12, '740145', 'awVNKRw2nP2KB2tCNitM7XKaTDshnu8f', '10d0b55e0ce96e1ad711adaac266c9200cbc27e4', '740145telkom.co.id', '', 2, NULL, NULL);
INSERT INTO `users` VALUES (13, '790054', '4TsFFBegx8XPJuv-qJe7p3XnpqnlYxCz', 'ec72d94da376195ee002de99b1577e842a1c6bf5', '790054telkom.co.id', '', 2, NULL, NULL);
INSERT INTO `users` VALUES (14, '730372', 'Wahyudi', '608fa76c0e8b002cf6e2f27abe090ffad64b3317', '730372telkom.co.id', 'NIK', 2, NULL, NULL);
INSERT INTO `users` VALUES (15, '930071', '6IP8uu0m-y8IzX6On9jgUHmR0kVOjr8W', '', '930071telkom.co.id', 'NIK', 2, NULL, NULL);
INSERT INTO `users` VALUES (17, '920207', 'LnSDJ1xCwB_p-w52VN3zG3Q5kDyjwO2l', '66eb3341bb7355af3b6294f78ba579156eb19674', '920207telkom.co.id', '', 2, NULL, NULL);
INSERT INTO `users` VALUES (19, '660302', 'Dwi', 'sadsa', '660302telkom.co.id', 'NIK', 2, NULL, NULL);
INSERT INTO `users` VALUES (20, '810086', 'Anggun', 'w', '810086telkom.co.id', 'NIK', 1, NULL, NULL);
INSERT INTO `users` VALUES (21, '640456', 'Yanti', 'w', '640456telkom.co.id', 'NIK', 2, NULL, NULL);
INSERT INTO `users` VALUES (22, '880029', 'Lisa', 'f', '880029telkom.co.id', 'NIK', 1, NULL, NULL);
INSERT INTO `users` VALUES (23, '642053', 'ridwan', 'sw', '642053telkom.co.id', 'NIK', 2, NULL, NULL);
INSERT INTO `users` VALUES (24, '910031', 'to4AXHKIqZQ0OE3xdLQ9EVAw1SpUBoo4', 'bdf0c4c5fa984b981c2d0f347f09d8b4bffc7c73', '910031telkom.co.id', 'NIK', 2, NULL, NULL);
INSERT INTO `users` VALUES (25, '641970', 'asd', '3f44e3460d8a1011db1acb092bdae72162063fe5', '641970telkom.co.id', 'NIK', 1, NULL, NULL);
INSERT INTO `users` VALUES (26, '631990', 'Herdi', 'sssd', '631990telkom.co.id', 'NIK', 2, NULL, NULL);
INSERT INTO `users` VALUES (27, '930309', 'Galih', 'Galih', '930309@telkom.co.id', 'NIK', 1, NULL, NULL);
INSERT INTO `users` VALUES (28, '790031', 'Imam', 'Imam', '790031@telkom.co.id', 'NIK', 1, NULL, NULL);
INSERT INTO `users` VALUES (29, 'fahmi', '', 'b3f421c567b0d560d5865d9c96bbaff2cd336472', 'fahmi.hilmansyah@gmail.com', '', 1, NULL, NULL);
INSERT INTO `users` VALUES (32, '840137', '', '', '840137@telkom.co.id', 'NIK', 2, NULL, NULL);
INSERT INTO `users` VALUES (33, '461970', '', 'c72f3db97e95792f62df4c2ca2cfdb64fb860160', '', '', 2, NULL, NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
