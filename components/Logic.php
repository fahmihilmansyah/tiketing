<?php namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\Html;

class Logic extends Component
{
    public static function arrArtikel()
    {
        $artikel = \app\models\Artikel::find()
                        ->select(['id','judul'])
                        ->orderBy('id')
                        ->asArray()
                        ->all();

        $data=[];
        $data[''] = "Pilih Judul Artikel ...";
        foreach ($artikel as $key => $value) {
            $data[$value['judul']]=$value['judul'];
        }
        return $data;

    }

    public static function ambil_waktu($datetime)
	{
		$arr_date = explode(" ", $datetime);
	    $ind_date = self::indonesian_date($arr_date[0]);
	    return substr($arr_date[1],0,8);
	}
    
    public static function int_to_time($menit)
	{
		$second = (int) ((int) $menit)*60;
		$time = gmdate("H:i", $second);
		return $time;
	}
}
